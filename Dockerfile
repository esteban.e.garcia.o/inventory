FROM us.gcr.io/helpful-spider-180417/laravel:latest
# Added supervisor.conf
ADD docker/conf/supervisord.conf /etc/supervisord.conf
# Copy the Application
COPY ./database ./database
COPY .env.production composer.json composer.lock ./
# Rename the Environment file
RUN mv .env.production .env
COPY . ./
#Touch the log file
RUN touch storage/logs/laravel.log config/analytics.php config/dhl.php config/mail.php config/services.php
RUN chown -R www-data:www-data storage bootstrap/cache config/analytics.php config/dhl.php config/mail.php config/services.php
#Delete Unecessary files and directories
RUN rm -rf docker html
#Local Build Instructions
# COPY auth.json auth.json
# RUN composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts --no-dev && \
#     yarn install
#Install Yarn Dependencies
#RUN yarn run prod
#Add Crontab File
ADD docker/conf/crontab /etc/crontabs/root
RUN chmod 0644 /etc/crontabs/root
#Add  Start Scripts for start
RUN chmod 755 start.sh
CMD ["/bin/bash", "-c", "./start.sh"]
