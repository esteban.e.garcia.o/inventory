<?php

namespace App\Exports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;
class ProductExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    

    public function collection()
    {
        $dataProducts = DB::table('products')
        ->join('categories', 'categories.id', '=', 'products.category_id')
        ->join('units', 'units.id', '=', 'products.unit_id')
        ->select('code','products.name','price','stock','min_stock','barcode','description','products.status','categories.name as cat','units.name as uni')
        ->get();

        
        return $dataProducts;
         //Product::->get();
    }

    public function headings():array
    {
        return [
            'codigo',
            'nombre_del_producto',
            'precio',
            'existencia',
            'minimo_de_existencia',
            'codigo_de_barra',
            'descripcion',
            'estatus',
            'categoria',
            'unidad_de_medida'
            
        ];
    }
}
