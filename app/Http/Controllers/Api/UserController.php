<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;

class UserController extends Controller
{
    protected $resourceName = 'users';

    protected $resourceClass = \App\Models\User::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        

    }

    public function datatables() {
        $items = $this->resourceClass::orderDesc()->get();        
        $datatable = DataTables::collection($items);
        $this->columns($datatable);
        $datatable->rawColumns(['actions']);
        return $datatable->make(true);
    }

    private function columns(&$datatable) {
        $datatable->addColumn('longname', function ($item) {
            return $item->longname;
        })->editColumn('status', function ($item) {
            return __('general.' . ($item->status ? 'active' : 'inactive'));
        })->editColumn('created_at', function ($item) {
            return $item->created_at->format("d/m/Y H:i");
        })->editColumn('actions', function ($item) {
            return view('common.actions', [
                "edit" => true,
                "routeEdit" => route("admin.$this->resourceName.edit", ['user' => $item->id]),
            ])->render();
        });
    }

    public function checkLogin(Request $request) {
        if ($user = User::active()->where('user_name', $request->user_name)->with('roles')->whereHas('roles', function ($query){
            return $query->where("roles.name", "provider");
        })->first()) {
            if (\Hash::check($request->password, $user->password, [])) {
                return response()->json(["provider" => $user]);
            }
            return response()->json(["error" => true, "message" => 'Usuario y/o contraseña son incorrecto']);
        }
        return response()->json(["error" => true, "message" => 'El usuario no existe']);
    }

    public function dailyInformation(Request $request) {
        if ($user = User::active()->where('id', $request->id)->with('roles')->whereHas('roles', function ($query){
            return $query->where("roles.name", "provider");
        })->first()) {
            $products = $user->products()->active()->get();
            $clients = \App\Models\Client::active()->get();
            $deliveries = $user->deliveries()->with(['payments', 'client'])->credit()->pending()->get()->map(function ($delivery) {
                $delivery->created = $delivery->created_at->format('d/m/Y');
                $delivery->payment =  (doubleval($delivery->payments()->sum('payment')) + doubleval($delivery->initial_payment));
                return $delivery;
            });
            return response()->json(["products" => $products, "clients" => $clients, "deliveries" => $deliveries]);
        }
        return response()->json(["error" => true, "message" => 'El usuario no existe']);
    }

    public function products(Request $request) {
        if ($user = User::active()->where('id', $request->id)->with('roles')->whereHas('roles', function ($query){
            return $query->where("roles.name", "provider");
        })->first()) {
            $products = $user->products()->active()->get();            
            return response()->json(["products" => $products]);
        }
        return response()->json(["error" => true, "message" => 'El usuario no existe']);
    }

    public function deliveries(Request $request) {
        if ($user = User::active()->where('id', $request->id)->with('roles')->whereHas('roles', function ($query){
            return $query->where("roles.name", "provider");
        })->first()) {
            $deliveries = $user->deliveries()->with('client')->where('created', $request->date)->get();
            return response()->json(["deliveries" => $deliveries]);
        }
        return response()->json(["error" => true, "message" => 'El usuario no existe']);
    }

}
