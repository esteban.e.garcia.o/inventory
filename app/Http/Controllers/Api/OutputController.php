<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
class OutputController extends Controller
{
    protected $resourceName = 'outputs';

    protected $resourceClass = \App\Models\Output::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

    public function datatables() {
        $items = $this->resourceClass::type()->orderDesc()->get();        
        $datatable = DataTables::collection($items);
        $this->columns($datatable);
        $datatable->rawColumns(['actions']);
        return $datatable->make(true);
    }

    private function columns(&$datatable) {
        $datatable->editColumn('created_by', function ($item){
                return $item->manager->longname;
            })->editColumn('created_at', function ($item) {
                return $item->created_at->format("d/m/Y H:i");
            })->editColumn('user_id', function ($item) {
                return $item->provider->longname;
            })->editColumn('actions', function ($item) {
                return view('common.actions', [
                    "see" => true,
                    "title" => __('control.see_products'),
                    "seeEvent" => "window.seeProducts(this)",
                    "items" => $item->products()->with(['category', 'unit'])->get()
                ])->render();
            });
    }

    public function store(Request $request) {
        if ($request->has('products')) {
            if ($user = \App\Models\User::find($request->user_id)) {
                \DB::transaction(function () use($request, $user){
                    $output = $this->resourceClass::create([
                        "id" => \Uuid::generate(4)->string,
                        "type" => "O",
                        "created_by" => auth()->id(),
                        "user_id" => $user->id
                    ]);
                    foreach ($request->products as $item) {
                        if ($product = \App\Models\Product::find($item['id'])) {
                            $stock = $product->stock;
                            $product->stock = ($stock - $item['quantity']);
                            $product->save();
                            $output->products()->attach($product,["id" => \Uuid::generate(4)->string, "quantity" => $item['quantity']]);
    
                            if ($productUser = $user->products()->find($product->id)) {
                                $quantity = $productUser->pivot->quantity;
                                $productUser->pivot->previous_quantity = $quantity;
                                $productUser->pivot->quantity = ($quantity + $item['quantity']);
                                $productUser->pivot->save();
                            } else {
                                $user->products()->attach($product,[
                                    "id" => \Uuid::generate(4)->string, 
                                    "quantity" => $item['quantity'],
                                    "previous_quantity" => 0
                                ]);
                            }
                        }
                    } 
                });                
                return response()->json(["message" => __('control.output_created', ['name' => $user->longname])]);
            }
            return response()->json(["error" => true, "message" => __('control.user_not_found')]);
        }
        return response()->json(["error" => true, "message" => __('control.products_not_exists')]);
    }

    public function signature(Request $request, $id) {        
        if ($user = \App\Models\User::find($id)) {
            if (\Hash::check($request->password, $user->password, [])) {
                return response()->json(["message" => 'ok']);
            }            
            return response()->json(["message" => 'Contraseña incorrecta', 'error' => true]);
        }
        return response()->json(["error" => true, "message" => __('control.user_not_found')]);
    }
}
