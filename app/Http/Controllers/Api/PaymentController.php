<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
class PaymentController extends Controller
{
    protected $resourceName = 'payments';

    protected $resourceClass = \App\Models\Payment::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        

    }

    public function store(Request $request) {
        if ($item = \App\Models\Delivery::where('id', $request->delivery_id)->credit()->pending()->first()) {            
            if ($user = \App\Models\User::find($request->user_id)) {
                \DB::transaction(function () use($request, $user, $item){                    
                    if (!$this->resourceClass::find($request->id)) {
                        $item->payments()->create([
                            "id" => $request->id,
                            "user_id" => $user->id,
                            "client_id" => $request->client_id,
                            "payment" => $request->payment,
                            "created" => date('Y-m-d')
                        ]);
                        $total = $item->payments()->sum('payment');
                        if ((doubleval($total) + doubleval($item->initial_payment)) >= doubleval($item->total)) {
                            $item->status = "F";
                            $item->save();                        
                        }
                    }                    
                });  
                if ($item->status == "F")
                    return response()->json(["message" => __('delivery.paid_off')]);
                return response()->json(["message" => __('delivery.payment_creatd')]);
            }        
            return response()->json(["error" => true, "message" => __('control.user_not_found')]);  
        }
        return response()->json(["error" => true, "message" => __('delivery.delivery_not_found')]);
    }

    public function userCredit(Request $request, $user_id, $date) {
        if ($user = \App\Models\User::find($request->user_id)) {            
            $payments = \App\Models\Payment::with('client')->where('user_id', $user->id)->where('created', $date)->get();                
            $total = \App\Models\Delivery::where('user_id', $user->id)->where('created', $date)->sum('initial_payment');
            return response()->json(["payments" => $payments, 'total' => $total]);
        }        
        return response()->json(["error" => true, "message" => __('control.user_not_found')]);
    }
}
