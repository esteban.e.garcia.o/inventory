<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
class CategoryController extends Controller
{
    protected $resourceName = 'categories';

    protected $resourceClass = \App\Models\Category::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        

    }

    public function datatables() {
        $items = $this->resourceClass::orderDesc()->get();        
        $datatable = DataTables::collection($items);
        $this->columns($datatable);
        $datatable->rawColumns(['actions']);
        return $datatable->make(true);
    }

    private function columns(&$datatable) {
        $datatable->editColumn('status', function ($item) {
                return __('general.' . ($item->status ? 'active' : 'inactive'));
            })->editColumn('created_at', function ($item) {
                return $item->created_at->format("d/m/Y H:i");
            })->editColumn('actions', function ($item) {
            return view('common.actions', [
                "edit" => true,
                "routeEdit" => route("admin.$this->resourceName.edit", ['category' => $item->id]),
            ])->render();
        });
    }
}
