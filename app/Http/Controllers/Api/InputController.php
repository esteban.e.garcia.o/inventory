<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
class InputController extends Controller
{
    protected $resourceName = 'inputs';

    protected $resourceClass = \App\Models\Input::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

    public function datatables() {
        $items = $this->resourceClass::type()->orderDesc()->get();        
        $datatable = DataTables::collection($items);
        $this->columns($datatable);
        $datatable->rawColumns(['actions']);
        return $datatable->make(true);
    }

    private function columns(&$datatable) {
        $datatable->editColumn('created_by', function ($item){
                return $item->manager->longname;
            })->editColumn('created_at', function ($item) {
                return $item->created_at->format("d/m/Y H:i");
            })->editColumn('actions', function ($item) {
                return view('common.actions', [
                    "see" => true,
                    "title" => __('control.see_products'),
                    "seeEvent" => "window.seeProducts(this)",
                    "items" => $item->products()->with(['category', 'unit'])->get()
                ])->render();
            });
    }

    public function store(Request $request) {
        if ($request->has('products')) {
            \DB::transaction(function () use($request){
                $material = null;
                if ($request->has('material_id')) {
                    $material = \App\Models\Material::find($request->material_id);
                }
                $input = $this->resourceClass::create([
                    "id" => \Uuid::generate(4)->string,
                    "type" => "I",
                    "created_by" => auth()->id(),
                    "material_id" => (!is_null($material) ? $material->id : null)
                ]);
                $bales = 0;
                $total = 0;
                foreach ($request->products as $item) {
                    if ($product = \App\Models\Product::find($item['id'])) {
                        $stock = $product->stock;
                        $product->stock = ($stock + $item['quantity']);
                        $product->save();
                        $input->products()->attach($product,["id" => \Uuid::generate(4)->string, "quantity" => $item['quantity']]);
                        $bales+= \doubleval($item['quantity']);
                        if (\doubleval($product->kilos) > 0)
                            $total+= (\doubleval($item['quantity']) * \doubleval($product->kilos));
                    }
                }           
                if ($material) {
                    $remaining = $material->remaining;
                    $material->remaining = (\doubleval($remaining) - $total);
                    $material->bales = $bales;
                    $material->save();
                }     
            });  
            return response()->json(["message" => __('control.input_created')]);          
        }
        return response()->json(["error" => true, "message" => __('control.products_not_exists')]);
    }
}
