<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
class MaterialController extends Controller
{
    protected $resourceName = 'materials';

    protected $resourceClass = \App\Models\Material::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        

    }

    public function datatables() {
        $items = $this->resourceClass::orderDesc()->get();        
        $datatable = DataTables::collection($items);
        $this->columns($datatable);
        $datatable->rawColumns(['actions']);
        return $datatable->make(true);
    }

    private function columns(&$datatable) {
        $datatable->editColumn('status', function ($item) {
                return __('general.' . ($item->status ? 'active' : 'inactive'));
            })->editColumn('created_at', function ($item) {
                return $item->created_at->format("d/m/Y H:i");
            })->editColumn('total', function ($item) {
                return number_format($item->total, 2);
            })->editColumn('remaining', function ($item) {
                return number_format($item->remaining, 2);
            })->editColumn('bales', function ($item) {
                return number_format($item->bales, 2);
            })->editColumn('actions', function ($item) {
            return view('common.actions', [
                "edit" => true,
                "routeEdit" => route("admin.$this->resourceName.edit", ['material' => $item->id]),
                "see" => true,
                "title" => __('general.see_inputs'),
                "seeEvent" => "window.seeInputs(this)",
                "items" => $item->inputs()->with('manager')->get()->map(function ($item){
                    $item->created = $item->created_at->format("d/m/Y H:i");
                    $item->quantity = $item->products()->sum('productables.quantity');
                    return $item;
                })
            ])->render();
        });
    }
}
