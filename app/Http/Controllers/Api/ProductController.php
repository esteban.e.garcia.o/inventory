<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use App\Models\Delivery;
use Carbon\Carbon;
class ProductController extends Controller
{
    protected $resourceName = 'products';

    protected $resourceClass = \App\Models\Product::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        

    }

    public function datatables() {
        $items = $this->resourceClass::orderDesc()->get();        
        $datatable = DataTables::collection($items);
        $this->columns($datatable);
        $datatable->rawColumns(['actions']);
        return $datatable->make(true);
    }

    private function columns(&$datatable) {
        $datatable->editColumn('price', function ($item) {
               return "$ " . number_format($item->price, 2);
            })
             ->editColumn('category', function ($item) {
                return $item->category->name;
            })->editColumn('unit', function ($item) {
                return $item->unit->name;
            })->editColumn('status', function ($item) {
                return __('general.' . ($item->status ? 'active' : 'inactive'));
            })->editColumn('created_at', function ($item) {
                return $item->created_at->format("d/m/Y H:i");
            })->editColumn('actions', function ($item) {
            return view('common.actions', [
                "edit" => auth()->user()->can('admin.products.update'),
                "routeEdit" => route("admin.$this->resourceName.edit", ['product' => $item->id]),
            ])->render();
        });
    }

    public function datatablesModal(Request $request) {
        $items = $this->resourceClass::active()->with('category')->whereHas('category', function ($query) use($request){
            if ($request->has('materials')) {
                return $query->whereRaw('lower(name) like (?)', '%papel%');
            }
        })->orderDesc()->get();        
        $datatable = DataTables::collection($items);
        $this->columnsModal($datatable);
        $datatable->rawColumns(['actions']);
        return $datatable->make(true);
    }

    private function columnsModal(&$datatable) {
        $datatable->editColumn('category', function ($item) {
                return $item->category->name;
            })->editColumn('unit', function ($item) {
                return $item->unit->name;
            })->editColumn('created_at', function ($item) {
                return $item->created_at->format("d/m/Y H:i");
            })->editColumn('actions', function ($item) {
            return view('common.actions', [
                "check" => true,
                "item" => $item,
                "checkEvent" => "window.addProduct(this)"
            ])->render();
        });
    }

    public function productsProvider(Request $request, $user_id) {
        if ($user = User::find($user_id)) {     
            if (!\App\Models\Cashcut::where('user_id', $user->id)->where('created', $request->date)->exists()) {
                $products = $user->products()->with(['category', 'unit'])->get();
                if (\sizeof($products) > 0) {
                    $deliveries = Delivery::where(["created" => $request->date, "user_id" => $user->id])->get();
                    $products->filter(function ($product) use($deliveries){
                        $quantity = 0;
                        $quantityCredit = 0;
                        foreach ($deliveries as $delivery) {                        
                            $sum = \App\Models\Productable::where([
                                "productable_id" => $delivery->id, 
                                "productable_type" => get_class($delivery),
                                "product_id" => $product->id
                            ])->sum('quantity');   
                            if ($delivery->type == "N")
                                $quantity += $sum;
                            else
                                $quantityCredit += $sum;
                        }
                        $product->quantity = $quantity;
                        $product->quantity_credit = $quantityCredit;
                        $product->total = ($product->price * $quantity);
                        return $product;
                    });
                    $sum = Delivery::where(["created" => $request->date, "user_id" => $user->id])->sum('initial_payment');
                    $sumPayments = \App\Models\Payment::where(["created" => $request->date, "user_id" => $user->id])->sum('payment');
                    $sumTotalDNormal = Delivery::normal()->where(["created" => $request->date, "user_id" => $user->id])->sum('total');
                }            
                return response()->json(['products' => $products, 'total_deliveries_normal' => $sumTotalDNormal, 'total_deliveries_credit' => (doubleval($sum) + doubleval($sumPayments))]);
            }                   
            return response()->json(["error" => true, "message" => __('delivery.cash_cut_exists_provider_to_day', ['name' => $user->longname, 'date' => $request->date])]);
        }
        return response()->json(["error" => true, "message" => __('control.user_not_found')]);
    }

    public function soldFilter(Request $request) {
        if ($request->has('products') && $request->has('date_from') && $request->has('date_to') && $request->has('type') && $request->has('status')) {                 
            $products = $this->resourceClass::with(['unit', 'category'])->whereIn('id', array_column($request->products, 'id'))->get();                     
            $deliveries = \App\Models\Delivery::where(['type' => $request->type, 'status' => $request->status])
                    ->whereDate('created', '>=', $request->date_from)
                    ->whereDate('created', '<=', $request->date_to)->get()->pluck('id');
            $products = $products->map(function ($product) use($request, $deliveries){                
                $product->sold = \App\Models\Productable::where(['productable_type' => \App\Models\Delivery::class, 'product_id' => $product->id])
                    ->whereIn('productable_id', $deliveries)
                    ->whereDate('created_at', '>=', new Carbon($request->date_from))
                    ->whereDate('created_at', '<=', new Carbon($request->date_to))
                    ->sum('quantity');
                $product->total = ($product->sold * $product->price);
                return $product;
            });
            return response()->json(['products' => $products]);            
        }
        return response()->json(["error" => true, "message" => __('Parametros no existen')]);
    }
}
