<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
class CashcutController extends Controller
{
    protected $resourceName = 'cashcuts';

    protected $resourceClass = \App\Models\Cashcut::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        

    }

    public function store(Request $request) {
        if ($request->has('products')) {            
            if ($user = \App\Models\User::find($request->user_id)) {             
                \DB::transaction(function () use($request, $user){                    
                    $cashcut = $this->resourceClass::create([
                        "id" => \Uuid::generate(4)->string,
                        "user_id" => $user->id,
                        "created_by" => auth()->id(),
                        "total_payments" => $request->total_payments,
                        "total_provider" => $request->total_provider,
                        "created" => $request->date
                    ]);
                    foreach ($request->products as $item) {
                        if ($product = $user->products()->find($item['id'])) {                            
                            if ($item['quantity'] != "0") {
                                $cashcut->products()->attach($product,[
                                    "id" => \Uuid::generate(4)->string, 
                                    "quantity" => $item['quantity'],
                                    "price" => $item['price']
                                ]);
                            }
                        }
                    }                
                });  
                return response()->json(["message" => __('delivery.cashcut_created')]);
            }        
            return response()->json(["error" => true, "message" => __('control.user_not_found')]);  
        }
        return response()->json(["error" => true, "message" => __('control.products_not_exists')]);
    }

    public function products(Request $request, $user_id) {
        if ($user = User::find($user_id)) {     
            if ($item = \App\Models\Cashcut::where('user_id', $user->id)->where('created', $request->date)->first()) {
                $products = $item->products()->with(['category', 'unit'])->get();    
                $sumTotalDNormal = \App\Models\Delivery::normal()->where(["created" => $request->date, "user_id" => $user->id])->sum('total');
                $item->total_deliveries_normal = $sumTotalDNormal;       
                return response()->json(['products' => $products, 'item' => $item]);
            }                   
            return response()->json(["error" => true, "message" => __('delivery.cashcurt_not_exists')]);
        }
        return response()->json(["error" => true, "message" => __('control.user_not_found')]);
    }
}
