<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DataTables;


class ClientController extends Controller
{
    protected $resourceName = 'clients';

    protected $resourceClass = \App\Models\Client::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        

    }

    public function datatables() {
        $items = $this->resourceClass::orderDesc()->get();        
        $datatable = DataTables::collection($items);
        $this->columns($datatable);
        $datatable->rawColumns(['actions']);
        return $datatable->make(true);
    }

    private function columns(&$datatable) {
        $datatable->editColumn('status', function ($item) {
                return __('general.' . ($item->status ? 'active' : 'inactive'));
            })->editColumn('created_at', function ($item) {
                return $item->created_at->format("d/m/Y H:i");
            })->editColumn('actions', function ($item) {
            return view('common.actions', [
                "edit" => true,
                "routeEdit" => route("admin.$this->resourceName.edit", ['client' => $item->id]),
            ])->render();
        });
    }

    public function store(Request $request) {
        $item = $this->resourceClass::make($request->all());       
        $validator = Validator::make(
          $request->all(),
          $item->rules ? $item->rules : [],[]
        );   
            
        if($validator->fails()) {
          return response()->json($validator->errors()->toArray(), 400);
        }
            
        if ($item->save()) { 
          return response()->json(["message" => __('control.clientSuccess')]);  
        }
     return response()->json(["error" => true, "message" => __('control.clientError')]);  
    }

    public function update(Request $request,$id) {
        
        $item = $this->resourceClass::find($id);      
        
            
        //$item->id = \Uuid::generate(4)->string;
        $values = $request->all();        
        if ($item->update($values)) { 
          return response()->json(["message" => __('control.clientUpdate')]);  
        }
     return response()->json(["error" => true, "message" => __('control.clientUpdateError')]);  
    }
}
