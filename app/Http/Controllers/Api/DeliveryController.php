<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
class DeliveryController extends Controller
{
    protected $resourceName = 'deliveries';

    protected $resourceClass = \App\Models\Delivery::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        

    }

    public function datatables(Request $request) {
        $items = $this->resourceClass::{$request->type}()->where(function ($query) use($request){
            if ($request->has('date') && $request->has('user_id')) {
                return $query->where('created', $request->date)->where('user_id', $request->user_id);
            }            
        })->orderDesc()->get();        
        $datatable = DataTables::collection($items);
        $this->columns($datatable, $request);
        $datatable->rawColumns(['actions']);
        return $datatable->make(true);
    }

    private function columns(&$datatable, $request) {
        $datatable->editColumn('provider', function ($item) {
                return $item->provider->longname;
            })->editColumn('client', function ($item) {
                return $item->client->longname;
            })->editColumn('status', function ($item) {
                return __('delivery.' . ($item->status == 'P' ? 'pending' : 'finish'));
            })->editColumn('initial_payment', function ($item) {
                return  "$ " . number_format($item->initial_payment, 2);
            })->editColumn('total', function ($item) {
                return  "$ " . number_format($item->total, 2);
            })->editColumn('payment', function ($item) {
                return  "$ " . number_format(doubleval($item->payments()->sum('payment')) + doubleval($item->initial_payment), 2);
            })->editColumn('created', function ($item) {
                return $item->created_at->format("d/m/Y");
            })->editColumn('actions', function ($item) use($request){
            return view('common.actions', [
                "see" => true,
                "title" => __('control.see_products'),
                "seeEvent" => "window.seeProducts(this)",
                "items" => $item->products()->with(['category', 'unit'])->get(),
                "credit" => ($request->type == "credit"),
                "creditEvent" => "window.seeCredits(this, $item->initial_payment)",
                "itemsCredit" => $item->payments()->with('provider')->get()
            ])->render();
        });
    }

    public function store(Request $request) {
        if ($request->has('delivery')) {
            $deliveryRequest = $request->delivery;
            if ($user = \App\Models\User::find($deliveryRequest['user_id'])) {
                if (!\App\Models\Cashcut::where('user_id', $user->id)->where('created', date('Y-m-d'))->exists()) {
                    \DB::transaction(function () use($deliveryRequest, $user){                    
                        if (!$this->resourceClass::find($deliveryRequest['id'])) {
                            $delivery = $this->resourceClass::create([
                                "id" => $deliveryRequest['id'],
                                "user_id" => $user->id,
                                "client_id" => $deliveryRequest['client_id'],
                                "type" => $deliveryRequest['type'],
                                "initial_payment" => $deliveryRequest['initial_payment'],
                                "status" => ($deliveryRequest['type'] == "N" ? "F" : "P"),
                                "created" => date("Y-m-d"),
                                "total" => $deliveryRequest['total']
                            ]);
                            
                            foreach ($deliveryRequest['products'] as $item) {
                                if ($product = $user->products()->find($item['id'])) {
                                    $quantity = $product->pivot->quantity;
                                    $product->pivot->quantity = ($quantity - $item['quantity']);
                                    $product->pivot->save();
                                    $delivery->products()->attach($product,["id" => \Uuid::generate(4)->string, "quantity" => $item['quantity'], 'price' => $product->price]);                            
                                }
                            }        
                        }
                    });  
                    return response()->json(["message" => __('delivery.delivery_created')]);
                }
                return response()->json(["error" => true, "message" => __('delivery.cash_cut_exists_provider_to_day', ['name' => $user->longname, 'date' => date('d/m/Y')]) . ', puedes intentarlo mañana a primera hora.']);                
            }        
            return response()->json(["error" => true, "message" => __('control.user_not_found')]);  
        }
        return response()->json(["error" => true, "message" => __('delivery.delivery_not_exist')]);
    }
}
