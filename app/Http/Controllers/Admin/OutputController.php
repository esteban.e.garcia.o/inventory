<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use \App\Models\User;
class OutputController extends Controller
{
    protected $resourceName = 'outputs';
    protected $resourceClass = \App\Models\Output::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}    

    protected function compactToView($item = null){
        $providers = [];
        $users = User::active()->with('roles')->whereHas('roles', function ($query){
            return $query->where("roles.name", "provider");
        })->get();
        
        foreach ($users as $item) {
            $providers[$item->id] = $item->longname;                                    
        }       
        $this->compacts['providers'] = $providers;        
    }
    
}
