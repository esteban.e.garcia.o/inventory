<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Material;
class InputController extends Controller
{
    protected $resourceName = 'inputs';
    protected $resourceClass = \App\Models\Input::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}    

    
    protected function compactToView($item = null){
        $materials = [];
        $users = Material::active()->where('remaining', '>', 0)->get();
        
        foreach ($users as $item) {
            $materials[$item->id] = $item->name;                                    
        }       
        $this->compacts['materials'] = $materials;        
    }    
}
