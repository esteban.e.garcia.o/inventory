<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\User;
class DeliveryController extends Controller
{
    protected $resourceName = 'deliveries';
    protected $resourceClass = \App\Models\Delivery::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}    

    protected function compactToView($item = null){
        $providers = [];
        $users = User::active()->with('roles')->whereHas('roles', function ($query){
            return $query->where("roles.name", "provider");
        })->get();
        
        foreach ($users as $item) {
            $providers[$item->id] = $item->longname;                                    
        }       
        $this->compacts['providers'] = $providers;        
    }
    
    public function deliveryCredit() {
        $type = 'credit';
        return view('admin.deliveries.credit', \compact('type'));
    }

    public function deliveryNormal() {
        $type = 'normal';
        return view('admin.deliveries.credit', \compact('type'));
    }

    public function getDeliveriesCredit() {
        $deliveries = $this->resourceClass::credit()->pending()->with('client')->get()->map(function ($delivery) {
            $delivery->should = (doubleval($delivery->total) - (doubleval($delivery->payments()->sum('payment')) + doubleval($delivery->initial_payment)));
            return $delivery;
        });
        return response()->json($deliveries);
    }
}
