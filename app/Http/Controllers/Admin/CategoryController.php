<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Role;
class CategoryController extends Controller
{
    protected $resourceName = 'categories';
    protected $resourceClass = \App\Models\Category::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}    

    protected function rulesValidations($item = null){
        $item->rules['name']= 'required|max:50|unique:categories,name,'.$item->id;
    }
    
    
}
