<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Role;
class UserController extends Controller
{
    protected $resourceName = 'users';
    protected $resourceClass = \App\Models\User::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}    

    protected function compactToView($item = null){
        $roles = [];
        foreach (Role::get() as $item) {
            $roles[$item->id] = $item->name;                                    
        }       
        $this->compacts['roles'] = $roles;
    }

    protected function beforeSave($item = null, $request, $insert = true) {
        if (!$insert && $item->phone !== $request->phone) {
            $item->password = \Hash::make($request->phone);
            $item->save();    
        } else
            $item->password = \Hash::make($request->phone);
    }

    protected function afterSave($item = null, $request) {
        if ($role = Role::find($request->role_id)) {
            $item->syncRoles($role);
        }
        if(is_null($item->api_token)){
            $item->api_token = \Str::random(60);
            $item->save();
        }
    }
}
