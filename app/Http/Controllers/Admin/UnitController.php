<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Role;
class UnitController extends Controller
{
    protected $resourceName = 'units';
    protected $resourceClass = \App\Models\Unit::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}    

    protected function rulesValidations($item = null){
        $item->rules['name']= 'required|max:50|unique:units,name,'.$item->id;
    }
    
    
}
