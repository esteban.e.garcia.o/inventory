<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Role;
class MaterialController extends Controller
{
    protected $resourceName = 'materials';
    protected $resourceClass = \App\Models\Material::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}  

    protected function beforeSave($item = null, $request, $insert = true) {
        if (!$insert && $item->bales == 0) {
            $item->remaining = $request->total;
            $item->save();    
        } else
            $item->remaining = $request->total;
    }
}
