<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Category;
use App\Models\Product;
use App\Models\Unit;
use App\Exports\ProductExport;
use App\Imports\ProductImport;
use Excel;
use Validator;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    protected $resourceName = 'products';
    protected $resourceClass = \App\Models\Product::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}    

    protected function compactToView($item = null){
        $categories = [];
        foreach (Category::active()->get() as $item) {
            $categories[$item->id] = $item->name;                                    
        }       
        $this->compacts['categories'] = $categories;

        $units = [];
        foreach (Unit::active()->get() as $item) {
            $units[$item->id] = $item->name;                                    
        }       
        $this->compacts['units'] = $units;
    }

    public function productsExport(){
        return Excel::download(new ProductExport,'lista-productos.xlsx');
    }

    public function productsImport(Request $request){
        if($request->hasFile('file')){
            $validator = Validator::make(
                array(
                    'file' => $request->file('file'),
                ),
                array(
                    'file' => 'file|max:5000|mimes:xlsx',
                )
            );
            if (!$validator->fails()) {
                $file=$request->file('file');
                Excel::import(new ProductImport,$file);
                return redirect()->route("admin.$this->resourceName.index", ["action" => "ok"]);
           }else{
                return redirect()->route("admin.$this->resourceName.index", ["action" => "error"])->withErrors($validator)->withInput();
           }      
        }
        return redirect()->route("admin.$this->resourceName.index", ["action" => "error"]);

    }

    public function productsSupply(){
        $products = $this->resourceClass::select('name','description','stock')->active()
                ->whereColumn('stock', '<=', 'min_stock')
                ->get();

        return response()->json($products);
    }

    protected function rulesValidations($item = null){
        $item->rules['code']= 'required|numeric|unique:products,code,'.$item->id;
        $item->rules['name']= 'required|max:100|unique:products,name,'.$item->id;
        $item->rules['barcode']= 'required|max:30|unique:products,barcode,'.$item->id;
    }

    public function soldFilter(Request $request) {
        return view("admin.$this->resourceName.sold");
    }
    
}
