<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\View;

class MainMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();
        if($user && is_null($user->api_token)){
            $user->api_token = \Str::random(60);
            $user->save();
        }
        \Menu::makeOnce('menus', function ($menu) {
            $menu->add('navigation.menu.administration', '#');

            $menu->get('navigation.menu.administration')
                ->add('navigation.menu.users', route('admin.users.index'))
                ->data('permission', 'admin.users.read')
                ->data('icon','users');
            
            $menu->get('navigation.menu.administration')
                ->add('navigation.menu.categories', route('admin.categories.index'))
                ->data('permission', 'admin.categories.read')
                ->data('icon','align-center');
            
            
            $menu->get('navigation.menu.administration')
                ->add('navigation.menu.units', route('admin.units.index'))
                ->data('permission', 'admin.units.read')
                ->data('icon','weight');

            $menu->get('navigation.menu.administration')
                ->add('navigation.menu.products', route('admin.products.index'))
                ->data('permission', 'admin.products.read')
                ->data('icon','box');            
            
            $menu->get('navigation.menu.administration')
                ->add('navigation.menu.inputs', route('admin.inputs.index'))
                ->data('permission', 'admin.inputs.read')
                ->data('icon','dolly-flatbed');            

            $menu->get('navigation.menu.administration')
                ->add('navigation.menu.outputs', route('admin.outputs.index'))
                ->data('permission', 'admin.outputs.read')
                ->data('icon','truck-moving');            

            $menu->get('navigation.menu.administration')
                ->add('navigation.menu.clients', route('admin.clients.index'))
                ->data('permission', 'mobile.clients.read')
                ->data('icon','user-circle'); 
            
            $menu->get('navigation.menu.administration')
                ->add('navigation.menu.materials', route('admin.materials.index'))
                ->data('permission', 'admin.materials.read')
                ->data('icon','box-open');
                
            $menu->add('navigation.menu.deliveries', '#');
            $menu->get('navigation.menu.deliveries')
                ->add('navigation.menu.of_day', route('admin.deliveries.index'))
                ->data('permission', 'mobile.deliveries.read')
                ->data('icon','truck-loading');    
            
            $menu->get('navigation.menu.deliveries')
                ->add('navigation.menu.cashcuts', route('admin.cashcuts.index'))
                ->data('permission', 'admin.cashcuts.read')
                ->data('icon','box-open');   
            
            $menu->get('navigation.menu.deliveries')
                ->add('navigation.menu.credit', route('admin.deliveries.credit.index'))
                ->data('permission', 'admin.payments.read')
                ->data('icon','credit-card');
                
            $menu->get('navigation.menu.deliveries')
                ->add('navigation.menu.normal', route('admin.deliveries.normal.index'))
                ->data('permission', 'admin.payments.read')
                ->data('icon','shopping-cart');    

        })->filter(function ($item) {            
            return \Auth::user()->can($item->data('permission'));
        });
        
        $menus = \Menu::get('menus');
        View::share(['menus' => $menus]);
        return $next($request);
    }
}
