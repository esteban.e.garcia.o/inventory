<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'sync roles and permissions';

    private $modelRole = \App\Models\Role::class;

    private $modelPermission = \App\Models\Permission::class;

    private $roles = [
        "root", "employee", "provider"
    ];

    private $permissionsRoot = [
        "admin.users.write", "admin.users.read", "admin.users.update", "admin.users.delete",        
        "admin.products.update",
    ];

    private $permissionsEmployee = [
        "admin.categories.write", "admin.categories.read", "admin.categories.update",  "admin.categories.delete", 
        "admin.units.write", "admin.units.read", "admin.units.update", "admin.units.delete", 
        "admin.products.write", "admin.products.read", "admin.products.delete", 
        "admin.inputs.write", "admin.inputs.read", "admin.inputs.update", "admin.inputs.delete", 
        "admin.outputs.write", "admin.outputs.read", "admin.outputs.update", "admin.outputs.delete", 
        "admin.deliveries.write", "mobile.deliveries.read", "mobile.deliveries.update",
        "admin.payments.read",
        "admin.cashcuts.write", "admin.cashcuts.read", "admin.cashcuts.update", "admin.cashcuts.delete",
        "admin.materials.write", "admin.materials.read", "admin.materials.update",  "admin.materials.delete", 
    ];

    private $permissionsProvider = [
        "mobile.clients.write", "mobile.clients.read", "mobile.clients.update", "mobile.clients.delete",
        "mobile.deliveries.write", "mobile.deliveries.read",
        "mobile.payments.write", "mobile.payments.read", 
        "mobile.pendings.read"
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->sync();
    }

    private function sync() {
        $this->storeRoles($this->roles);
        $permissionsRoot = [];

        $permissions = $this->storePermissions($this->permissionsProvider);
        $this->syncPermissionsByRole("provider", $permissions);
        $permissionsRoot = $permissions;

        $permissions = $this->storePermissions($this->permissionsEmployee);
        $this->syncPermissionsByRole("employee", $permissions);

        $permissionsRoot = array_merge($permissionsRoot, $permissions);

        $permissions = $this->storePermissions($this->permissionsRoot);
        $permissionsRoot = array_merge($permissionsRoot, $permissions);

        $this->syncPermissionsByRole("root", $permissionsRoot);

        $this->info("Roles and permissions created");
    }

    private function syncPermissionsByRole($name, $permissions = []) {
        if ($role = $this->modelRole::firstWhere("name", $name)) {
            $role->syncPermissions($permissions);
            $this->info("$name Role sync");
        }
    }

    private function storePermissions($permissions = []) {
        $permissionsReturn = [];
        foreach ($permissions as $permission) {
            $permissionsReturn[] = $permission;
            if (!$this->modelPermission::firstWhere("name", $permission)) {
                $this->modelPermission::create([
                    "id" => \Uuid::generate(4)->string,
                    "name" => $permission,
                    "guard_name" => "web"
                ]);
                $this->info("$permission permission created");
            }
        }
        return $permissionsReturn;
    }

    private function storeRoles($roles = []) {
        foreach ($roles as $role) {
            if (!$this->modelRole::firstWhere("name", $role)) {
                $this->modelRole::create([
                    "id" => \Uuid::generate(4)->string,
                    "name" => $role,
                    "guard_name" => "web"
                ]);
                $this->info("$role role created");
            }
        }
    }
}
