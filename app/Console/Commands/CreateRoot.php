<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateRoot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'root';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create user root';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {        
        $this->create();
    }

    private function create() {
        $this->info('Creating user root');
        $user = new \App\Models\User;
        $user->id = \Uuid::generate(4)->string;
        $user->user_name = $this->ask(__("general.command.user_name"), "root");
        $user->name = $this->ask(__("general.command.name"), "root");
        $user->surnames = $this->ask(__("general.command.surnames"), "root");
        $user->phone = $this->ask(__("general.command.phone"), "xxx xxx xxxx");
        $user->password = \Hash::make($this->ask(__("general.command.password"), "12345678"));
        $user->save();
        $this->info("User root created");
        $user->syncRoles('root');
        $this->info("Sync roles and permissions in the user created");
    }
}
