<?php

namespace App\Imports;

use App\Models\Product;
use App\Models\Category;
use App\Models\Unit;
use Maatwebsite\Excel\Concerns\{Importable, ToModel, WithHeadingRow, WithValidation};



class ProductImport implements ToModel, WithHeadingRow
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        $barcode= ($row['codigo_de_barra'] == "")?null:$row['codigo_de_barra'];
        
        $categoryId= Category::where("name","=",$row['categoria'])->select("id")->get();
            
        $unitId= Unit::where("name","=",$row['unidad_de_medida'])->select("id")->get();
        
        
        
        return new Product([
            "id"=>\Uuid::generate(4)->string,
            "code"=>$row['codigo'],
            "name"=>$row['nombre_del_producto'],
            "price"=>$row['precio'],
            "stock"=>$row['existencia'],
            "min_stock"=>$row['minimo_de_existencia'],
            "barcode"=>$barcode,
            "description"=>$row['descripcion'],
            "status"=>$row['estatus'],
            "category_id"=>$categoryId[0]->id,
            "unit_id"=>$unitId[0]->id
        ]);
        
        
    }
}
