<?php
namespace App\Traits;

/**
 * 
 */
trait ResourceControl
{
    //Nota: para el campo type se encuentra separado por dos tipos (I, O), lo cual quiere decir I = Inputs y O = Outputs
    public function scopeType($query) {
        return $query->where('type', $this->type);
    }

    public function manager() {
        return $this->belongsTo(\App\Models\User::class, 'created_by');
    }

    public function provider() {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }    
}
