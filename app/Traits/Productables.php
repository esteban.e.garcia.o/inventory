<?php
namespace App\Traits;

/**
 * 
 */
trait Productables
{
    public function products() {
        return $this->morphToMany(\App\Models\Product::class, 'productable')            
            ->withTimestamps()
            ->withPivot('id', 'quantity', 'previous_quantity', 'price');
    }  
}
