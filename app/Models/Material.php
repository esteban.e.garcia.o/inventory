<?php

namespace App\Models;

class Material extends Model
{
    protected $fillable = [
        'id', 'code', 'name', 'total', 'remaining', 'bales'
    ];

    public $rules = [
        'code' => 'required|integer|min:12',
        'name' => 'required|max:150',       
        'total' => 'required|integer|min:10', 
    ];

    public function inputs()
    {
        return $this->hasMany(Input::class);
    }
}
