<?php

namespace App\Models;

use Spatie\Permission\Models\Permission as PermissionModel;
use Spatie\Permission\Contracts\Permission as PermissionContract;

class Permission extends PermissionModel implements PermissionContract
{
    public $incrementing = false;

    public function getKeyType()
    {
        return 'string';
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'guard_name',
    ];
}
