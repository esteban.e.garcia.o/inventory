<?php

namespace App\Models;

use App\Traits\ResourceControl;
use App\Traits\Productables;

class Output extends Model
{
    use ResourceControl, Productables;    
    
    protected $table = "controls";

    protected $fillable = [
        'id', 'type', 'created_by', 'user_id'
    ];

    protected $type = 'O';
}
