<?php

namespace App\Models;
use App\Traits\Productables;

class Delivery extends Model
{
    use Productables;
    //Nota: el atributo type contiene dos parametros (N  y C) los cuales quieren decir N = Normal y C = Credit
    //Nota: el atributo status contiene dos parametros (P  y F) los cuales quieren decir P = Pending y F = Finish
    protected $fillable = [
        'id', 'user_id', 'client_id', 'type', 'initial_payment', 'status', 'created', 'total'
    ];   
    
    public function payments()
    {
        return $this->hasMany(\App\Models\Payment::class);
    }

    public function scopeCredit($query) {
        return $query->where('type', 'C');
    }

    public function scopeNormal($query) {
        return $query->where('type', 'N');
    }

    public function scopePending($query) {
        return $query->where('status', 'P');
    }

    public function provider() {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }  

    public function client() {
        return $this->belongsTo(\App\Models\Client::class, 'client_id');
    }
}
