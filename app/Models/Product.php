<?php

namespace App\Models;

class Product extends Model
{
    protected $fillable = [
        'id', 'code', 'name','price','stock','min_stock','description','barcode','status','category_id','unit_id', 'kilos'
    ];


    public $rules = [
        'code' => 'required|numeric|unique:products,code',
        'name' => 'required|max:100|unique:products,name',
        'price' => 'required|numeric',
        'stock' => 'required|integer',
        'barcode' => 'required|max:30|unique:products,barcode',
        'min_stock' => 'required|integer',
        'description' => 'required|max:300',
        'category_id' => 'required',
        'unit_id' => 'required'
    ];

    public function category() {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function unit() {
        return $this->belongsTo(\App\Models\Unit::class);
    }

    public function inputs() {
        return $this->morphedByMany(\App\Models\Input::class, 'productable')
            ->withTimestamps()
            ->withPivot('id', 'quantity');
    }

    public function outputs() {
        return $this->morphedByMany(\App\Models\Output::class, 'productable')
            ->withTimestamps()
            ->withPivot('id', 'quantity');
    }

    public function users() {
        return $this->morphedByMany(\App\Models\User::class, 'productable')
            ->withTimestamps()
            ->withPivot('id', 'quantity', 'previous_quantity');
    }
}
