<?php

namespace App\Models;

class Category extends Model
{    
    protected $fillable = [
        'id', 'name', 'status',
    ];


    public $rules = [
        'name' => 'required|max:50|unique:categories,name'
    ];

    public function products() {
        return $this->hasMany(\App\Models\Product::class);
    }

}
