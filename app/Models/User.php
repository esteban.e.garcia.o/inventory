<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\Productables;

class User extends Authenticatable
{
    use Notifiable, HasRoles, Productables;

    protected $guard_name = 'web';

    public $incrementing = false;

    public function getKeyType()
    {
        return 'string';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'user_name', 'name', 'surnames', 'phone', 'password', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime'
    ];

    public $rules = [
        'user_name' => 'required|max:10',
        'name' => 'required|max:200',
        'surnames' => 'required|max:200',
        'phone' => 'required|max:15',
    ];

    protected $appends = [
        "longname"
    ];

    public function getLongNameAttribute() {
        return "$this->name $this->surnames";
    }

    public function scopeActive($query) {
        return $query->where('status', true);
    }

    public function scopeOrderDesc($query) {
        return $query->orderBy('created_at', 'DESC');
    }

    public function deliveries() {
        return $this->hasMany(\App\Models\Delivery::class);
    }
}
