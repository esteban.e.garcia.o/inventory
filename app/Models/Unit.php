<?php

namespace App\Models;

class Unit extends Model
{
    protected $fillable = [
        'id', 'name', 'status',
    ];


    public $rules = [
        'name' => 'required|max:50|unique:units,name'
    ];

    public function products() {
        return $this->hasMany(\App\Models\Product::class);
    }
}
