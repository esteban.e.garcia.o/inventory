<?php

namespace App\Models;


class Client extends Model
{
    protected $fillable = [
        'id', 'longname', 'direction','phone','name_store','location','status','city'
    ];


    public $rules = [
        'longname' => 'required|max:200',
        'direction'=>'required|max:200',
        'phone'=>'required|numeric',
        'name_store'=>'required|max:200',
        'status'=>'required',
        'city'=>'required|max:200'
    ];

    protected $casts = [
        'location' => 'json',
        'created_at' => 'datetime'
    ];
}
