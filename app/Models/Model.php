<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as ModelMain;

class Model extends ModelMain
{
    public $incrementing = false;

    public function getKeyType()
    {
        return 'string';
    }

    public function scopeActive($query) {
        return $query->where('status', true);
    }

    public function scopeOrderDesc($query) {
        return $query->orderBy('created_at', 'DESC');
    }
}
