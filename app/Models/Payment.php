<?php

namespace App\Models;

class Payment extends Model
{
    protected $fillable = [
        'id', 'user_id', 'client_id', 'delivery_id', 'payment', 'created'
    ]; 

    public function provider() {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function client() {
        return $this->belongsTo(\App\Models\Client::class, 'client_id');
    }

    protected $cast = [
        "created" => "date"
    ];
}
