<?php

namespace App\Models;
use App\Traits\Productables;
use App\Traits\ResourceControl;

class Cashcut extends Model
{
    use Productables, ResourceControl;
    protected $fillable = [
        'id', 'user_id', 'created_by', 'total_payments', 'total_provider', 'created'
    ]; 
}
