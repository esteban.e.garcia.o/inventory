<?php

namespace App\Models;

use Spatie\Permission\Models\Role as RoleModel;
use Spatie\Permission\Contracts\Role as RoleContract;

class Role extends RoleModel implements RoleContract
{
    public $incrementing = false;

    public function getKeyType()
    {
        return 'string';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'guard_name',
    ];
}
