#Copy all public content to the nginx container
cp -r /var/www/public/ /web-data
#Set Cache
php /var/www/artisan route:cache
php /var/www/artisan optimize
#Set Permissions
chown -R www-data:www-data /var/www/storage
chown -R www-data:www-data /var/www/bootstrap/cache
#Readiness File
touch ready
#Start supervisord and services
/usr/bin/supervisord -n -c /etc/supervisord.conf
