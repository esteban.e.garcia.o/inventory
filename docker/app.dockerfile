FROM php:7.2-fpm-alpine
#Environment Variables
ENV fpm_conf /usr/local/etc/php-fpm.d/www.conf
ENV zz_conf /usr/local/etc/php-fpm.d/zz-docker.conf
ENV ACCEPT_EULA Y
# Install dev dependencies
RUN apk add --no-cache --virtual .build-deps \
    $PHPIZE_DEPS \
    curl-dev \
    imagemagick-dev \
    libtool \
    postgresql-dev \
    freetype \
    libpng \
    libjpeg-turbo \
    freetype-dev \
    libpng-dev \
    libsodium-dev \
    libzip-dev \
    libjpeg-turbo-dev

RUN apk add --no-cache --update \
    bash \
    g++ \
    git \
    curl \
    imagemagick \
    libc-dev \
    make \
    mysql-client \
    postgresql-client \
    nodejs \
    nodejs-npm \
    openssh-client \
    postgresql-libs \
    rsync \
    py-pip \
    libpng-dev
# Install PECL and PEAR extensions
RUN pecl install imagick
#PHP Extensions
RUN docker-php-ext-enable \
    imagick
RUN docker-php-ext-install \
    curl \
    iconv \
    mbstring \
    pdo \
    pdo_mysql \
    pdo_pgsql \
    zip \
    sodium \
    bcmath \
    sockets \
    ctype \
    json \
    tokenizer

RUN docker-php-ext-configure gd --with-gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-png-dir=/usr/include/
RUN docker-php-ext-install -j$(nproc) gd
RUN pip install --upgrade pip
# Supervisor config
RUN pip install wheel supervisor supervisor-stdout
# Override php-fpm config
RUN sed -i -e "s/;catch_workers_output\s*=\s*yes/catch_workers_output = yes/g" ${fpm_conf} && \
    sed -i -e "s/pm.max_children = 5/pm.max_children = 11/g" ${fpm_conf} && \
    sed -i -e "s/pm.start_servers = 2/pm.start_servers = 4/g" ${fpm_conf} && \
    sed -i -e "s/pm.min_spare_servers = 1/pm.min_spare_servers = 4/g" ${fpm_conf} && \
    sed -i -e "s/pm.max_spare_servers = 3/pm.max_spare_servers = 5/g" ${fpm_conf} && \
    sed -i -e "s/pm.max_requests = 500/pm.max_requests = 500/g" ${fpm_conf} && \
    sed -i -e "s/^;clear_env = no$/clear_env = no/g" ${fpm_conf} && \
    sed -i -e "s/^;request_terminate_timeout = 0/request_terminate_timeout = 0/g" ${fpm_conf} && \
    sed -i -e "s/;ping.response = pong/ping.response = pong/g" ${fpm_conf} && \
    sed -i -e "s/;ping.path = \/ping/ping.path = \/api\/ping/g" ${fpm_conf} && \
    sed -i -e "s/;pm.status_path = \/status/pm.status_path = \/api\/status/g" ${fpm_conf} && \
    sed -i -e "s/listen = 127.0.0.1:9000/listen = \/var\/run\/php-fpm.sock/g" ${fpm_conf} && \
    sed -i -e "s/;listen.owner = www-data/listen.owner = www-data/g" ${fpm_conf} && \
    sed -i -e "s/;listen.group = www-data/listen.group = www-data/g" ${fpm_conf} && \
    sed -i -e "s/;listen.mode = 0660/listen.mode = 0660/g" ${fpm_conf} && \
    echo 'php_admin_value[max_execution_time] = 900' >> ${fpm_conf}
#Override zz-docker config
RUN sed -i -e "s/listen = 9000/listen = \/var\/run\/php-fpm.sock/g" ${zz_conf} && \
    echo 'listen.mode = 0666' >> ${zz_conf} && \
    echo 'php_admin_value[max_execution_time] = 900' >> ${zz_conf}
#Copy The additional PHP config
COPY conf/php-conf.ini /usr/local/etc/php/conf.d/enterprise-php-conf.ini
#Install Yarn
RUN npm i -g yarn
# Install composer
RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV PATH="./vendor/bin:$PATH"
# Cleanup dev dependencies
RUN apk del -f .build-deps
# Setup working directory
WORKDIR /var/www
