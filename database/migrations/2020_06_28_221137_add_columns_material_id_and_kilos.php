<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsMaterialIdAndKilos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->double('kilos')->nullable()->default(0);
        });
        Schema::table('controls', function (Blueprint $table) {
            $table->uuid('material_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('kilos');
        });
        Schema::table('controls', function (Blueprint $table) {
            $table->dropColumn('material_id');
        });
    }
}
