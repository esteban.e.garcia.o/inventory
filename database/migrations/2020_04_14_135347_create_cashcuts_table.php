<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashcutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productables', function (Blueprint $table) {
            $table->double('price')->nullable();
        });
        Schema::create('cashcuts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('created_by')->index();
            $table->uuid('user_id')->index();
            $table->double('total_payments');
            $table->double('total_provider');
            $table->date('created')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productables', function (Blueprint $table) {
            $table->dropColumn('price');
        });
        Schema::dropIfExists('cashcuts');
    }
}
