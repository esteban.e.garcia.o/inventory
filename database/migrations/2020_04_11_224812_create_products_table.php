<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->integer('code')->index();
            $table->string("name")->unique()->index();
            $table->double("price");
            $table->integer("stock");
            $table->integer("min_stock");
            $table->string("barcode")->unique()->index();
            $table->text("description");
            $table->boolean('status')->default(1);
            $table->uuid("category_id")->index();
            $table->uuid("unit_id")->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
