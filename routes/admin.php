<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth', 'app.menu']], function () {    
    Route::resource('users', 'Admin\UserController');
    Route::resource('categories', 'Admin\CategoryController');
    Route::resource('units', 'Admin\UnitController');
    Route::resource('products', 'Admin\ProductController');
    Route::resource('inputs', 'Admin\InputController');
    Route::resource('outputs', 'Admin\OutputController');
    Route::resource('clients', 'Admin\ClientController');
    Route::resource('deliveries', 'Admin\DeliveryController');
    Route::resource('cashcuts', 'Admin\CashcutController');
    Route::resource('materials', 'Admin\MaterialController');
    Route::get('deliveries_credit', 'Admin\DeliveryController@deliveryCredit')->name('deliveries.credit.index');
    Route::get('deliveries_normal', 'Admin\DeliveryController@deliveryNormal')->name('deliveries.normal.index');
    //Exports
    Route::get('productsExport', 'Admin\ProductController@productsExport')->name('products.export');
    Route::post('productsImport', 'Admin\ProductController@productsImport')->name('products.import');
    //ProductsSupply
    Route::get('productsSupply', 'Admin\ProductController@productsSupply')->name('products.supply');  
    Route::get('deliveriescredit', 'Admin\DeliveryController@getDeliveriesCredit')->name('deliveries.credit');  
    Route::get('products/sold/filter', 'Admin\ProductController@soldFilter')->name('products.sold.filter');
     
});