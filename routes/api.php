<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['auth:api', 'cors']], function () { 
    Route::get('users/datatables', 'Api\UserController@datatables')->name('users.datatables');
    Route::get('categories/datatables', 'Api\CategoryController@datatables')->name('categories.datatables');
    Route::get('units/datatables', 'Api\UnitController@datatables')->name('units.datatables');
    Route::get('products/datatables', 'Api\ProductController@datatables')->name('products.datatables');   
    Route::post('products/sold/filter', 'Api\ProductController@soldFilter')->name('products.sold.filter');   
    Route::get('inputs/datatables', 'Api\InputController@datatables')->name('inputs.datatables');
    Route::get('ouputs/datatables', 'Api\OutputController@datatables')->name('outputs.datatables');
    Route::get('clients/datatables', 'Api\ClientController@datatables')->name('clients.datatables');    
    Route::get('materials/datatables', 'Api\MaterialController@datatables')->name('materials.datatables');    
    Route::get('products/modal/datatables', 'Api\ProductController@datatablesModal')->name('products.modal.datatables');
    Route::post('products/{user_id}/provider', 'Api\ProductController@productsProvider')->name('products.provider');
    Route::post('cashcuts/{user_id}/provider/products', 'Api\CashcutController@products')->name('cashcuts.products');
    Route::get('deliveries/payments/datatables', 'Api\DeliveryController@datatables')->name('deliveries.payments.datatables');        
    
    
    Route::post('inputs', 'Api\InputController@store')->name('inputs.store');
    Route::post('outputs', 'Api\OutputController@store')->name('outputs.store');
    Route::post('deliveries', 'Api\DeliveryController@store')->name('deliveries.store');
    Route::post('cashcuts', 'Api\CashcutController@store')->name('cashcuts.store');
    Route::post('payments', 'Api\PaymentController@store')->name('payments.store');
    Route::post('payments/user/{user_id}/{date}/credit', 'Api\PaymentController@userCredit')->name('payments.user.credit');
    
    Route::post('clients', 'Api\ClientController@store')->name('clients.store');
    Route::patch('clients/{client}', 'Api\ClientController@update');
    Route::post('outputs/provider/{id}/signature', 'Api\OutputController@signature')->name('outputs.provider.signature');
    //App mobile
    Route::post('provider/daily/information', 'Api\UserController@dailyInformation');
    Route::post('provider/products', 'Api\UserController@products');
    Route::post('provider/deliveries', 'Api\UserController@deliveries');
});    
Route::group(['middleware' => ['cors']], function () { 
    Route::post('provider/checkLogin', 'Api\UserController@checkLogin');    
});
