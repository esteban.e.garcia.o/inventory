<?php
return [
    "created_by" => "Creado por",
    "input" => "Entrada",
    "close" => "Cerrar",
    "accept" => "Aceptar",
    "select" => "Seleccionar",
    "selected" => "Seleccionado",
    "quantity" => "Cantidad",
    "delete" => "Eliminar",
    "select_product" => "Para continuar debes seleccionar al menos un producto",
    "loading" => "Cargando...",
    "have_not_prodcuts" => "¡Lo siento!<br>Pero aun no has agregado productos",
    "list_products_add" => "Lista de productos agregados",
    "only_numbers" => "Favor de ingresar sólo números",
    "quantity_exceeds" => "La cantidad rebasa la existencia del producto (existencia = :stock, cantidad = :quantity)",
    "necessary_quantity" => "Para continuar es necesario ingresar la cantidad en todos los productos",
    "see_products" => "Ver productos",
    "provider" => "Proveedor",
    "output" => "Salida",
    "select_provider" => "Seleccionar un proveedor",
    "input_created" => "Entrada creada",
    "products_not_exists" => "request products not exists",
    "user_not_found" => "user not found",
    "output_created" => "Salida creada. Nota: Con esta salida se aumento la existencia de los productos del proveedor \":name\"",
    "clientSuccess"=>"Cliente Creado Con exito",
    "clientError"=>"Cliente no creado",
    "clientUpdate"=>"Localizacion de cliente editado",
    "clientUpdateError"=>"Error al Modificar localización",
    "select_material" => "Selecciona una materia prima"
];