<?php
return [
    "menu" => [
        "administration" => "Administración",
        "users" => "Empleados",
        "categories" => "Categorías",
        "units" => "Unidades de medida",
        "products" => "Productos",
        "inputs" => "Entradas",
        "outputs" => "Salidas",
        "clients" => "Clientes",
        "deliveries" => "Entregas",
        "of_day" => "Del Día",
        "cashcuts" => "Cortes de caja",
        "credit" => "Crédito",
        "normal" => "Normales",
        "materials" => "Materia prima"
    ]
];
?>