import Swal from 'sweetalert2';
const translations = JSON.parse($("meta[name=translations]").attr("content"));
const urlPayments = $("meta[name=url-payments]").attr("content");
$(() => {    
    $("#btn-show-payments-modal").off('click');
    $("#btn-show-payments-modal").on('click', e => {
        window.loading('Cargando...');
        window.sendRequest({
            url: urlPayments,
            data: {},
            success: (response) => {
                if (response.error)
                    window.getMessage(response.message);
                else {                  
                    Swal.clickCancel();   
                    $("#credits-payments-modal").modal('show');
                    updatePayments(response.payments, response.total);
                }                
            },
            error: (e) => {
                console.log("Error", e);
                Swal.clickCancel();
            }
        }, {processData: true}); 
    });
});

window.seeProducts = (e) => {    
    const products = $(e).data('items');    
    updateProductsCredits(products);
    $("#products-credit-modal").modal('show');    
}

const updateProductsCredits = (products) => {
    $("#credits-products-table").find("tbody").html('');
    let total = 0;
    products.map((product) => {                
        $("#credits-products-table").find("tbody").append(`
            <tr>
                <td>${product.name}</td>
                <td>${product.stock}</td>
                <td>${product.category.name}</td>
                <td>${product.unit.name}</td>
                <td>${product.pivot.quantity}</td>
            </tr>
        `);
        total+= parseFloat(product.pivot.quantity) * parseFloat(product.price);
    }); 
    $("#total-sum-products").text(window.formaterCurrency(total));
}

window.seeCredits = (e, initial_payment) => {
    const credits = $(e).data('items');    
    updateCredits(credits, initial_payment);
    $("#credits-modal").modal('show');    
}

const updateCredits = (credits, initial_payment) => {
    $("#credits-table").find("tbody").html('');
    let total = 0;
    credits.map((credit) => {                
        $("#credits-table").find("tbody").append(`
            <tr>
                <td>${credit.provider.longname}</td>
                <td>${window.formaterCurrency(credit.payment)}</td>
                <td>${credit.created}</td>
            </tr>
        `);
        total+= parseFloat(credit.payment);
    }); 
    total+= parseFloat(initial_payment);
    $("#total-sum-credits").text(window.formaterCurrency(total) + ", Incluye " + window.formaterCurrency(initial_payment) + " del pago inicial");
}

const updatePayments = (payments, total_initial) => {
    $("#payments-table").find("tbody").html('');
    let total = 0;
    payments.map((credit) => {                
        $("#payments-table").find("tbody").append(`
            <tr>
                <td>${credit.client.longname}</td>
                <td>${window.formaterCurrency(credit.payment)}</td>
                <td>${credit.created}</td>
            </tr>
        `);
        total+= parseFloat(credit.payment);
    }); 
    $("#total-payments").text(window.formaterCurrency(total));
    $("#total-initial").text(window.formaterCurrency(total_initial));
    $("#total-general").text(window.formaterCurrency((total + parseFloat(total_initial))));
}