import Swal from 'sweetalert2';
const translations = JSON.parse($("meta[name=translations]").attr("content"));
const urlProductsProvider = $("meta[name=url-products-provider]").attr("content");
const urlProductsCashcuts = $("meta[name=url-products-cashcuts]").attr("content");
const urlDeliveriesCredits = $("meta[name=url-deliveries-credits]").attr("content") + '?date=:date&user_id=:user_id';

let products = [], totalPayments = 0, date;
$(() => {
    $("#btn-search-provider").off('click');
    $("#btn-search-provider").on('click', (e) => {
        const userId = $("#user_id").val();
        date = $("#date").val();
        if (userId) {
            window.loading(translations.loading);
            getProductsProvider(userId, date);
        } else
            window.getMessage(translations.select_provider);
    });

    $("#btn-save-cashcuts").off('click');
    $("#btn-save-cashcuts").on('click', (e) => {
        const userId = $("#user_id").val(), totalProvider = $("#input-total-delivered-provider").val();        
        if (userId) {
            if (products.length > 0) {
                if (totalProvider !== undefined && totalProvider !== "") {
                    const url = $(e.currentTarget).data('url'), date = $("#date").val();                    
                    window.loading(translations.loading);
                    saveCashCuts(url, {user_id: userId, total_provider: totalProvider, total_payments: totalPayments, date});
                } else
                    window.getMessage(translations.into_total_deliveries_provider);                        
            } else
                window.getMessage(translations.not_products_deliveries);                        
        } else
            window.getMessage(translations.select_provider);
    });
});

const saveCashCuts = (url, data) => {
    data.products = [];
    products.map((product) => data.products.push({id: product.id, price: product.total, quantity: product.quantity}));
    sendRequest(url,  data, (response) => {
        window.getMessage(response.message, 'success', () => {
            window.location.href = "/";
        });
    });
}

const sendRequest = (url, data = {}, callback) => {
    window.sendRequest({
        url: url,
        data: data,
        success: (response) => {
            if (response.error)
                window.getMessage(response.message);
            else {
                Swal.clickCancel();
                callback(response);
            }
                
        },
        error: (e) => {
            console.log("Error", e);
            Swal.clickCancel();
        }
    }, {processData: true});
}

const getProductsProvider = (userId, date = null) => {
    sendRequest((urlProductsCashcuts ? urlProductsCashcuts : urlProductsProvider).replace(':user_id', userId), {date}, (response) => {
        products = response.products;
        if (!urlProductsCashcuts) {
            let dataOne = [];
            products.map(product => dataOne.push([product.name, product.category.name, product.unit.name, product.pivot.quantity, product.quantity, product.quantity_credit]));
            window.initDatatablesWithOutAjax($("#datatables-products-provider-table"), dataOne); 
        }
        let dataTwo = [];
        products.map(product => dataTwo.push([
            product.name,
            product.category.name,
            product.unit.name,
            (!urlProductsCashcuts ? product.quantity : product.pivot.quantity),
            window.formaterCurrency((!urlProductsCashcuts ? product.total : product.pivot.price))
        ]));
        window.initDatatablesWithOutAjax($("#datatables-products-provider-clients-table"), dataTwo);               
        deliveriesCredit(response, userId);
    });
}

const deliveriesCredit = (response, userId) => {    
    let totalProvider = 0, totalCredit = response.total_deliveries_credit, totalNormal = response.total_deliveries_normal;    
    if (response.item) {
        totalCredit = parseFloat(response.item.total_payments);
        totalProvider = response.item.total_provider;
        totalNormal = response.item.total_deliveries_normal;
    } 
    $("#datatables-products-provider-clients-table-footer").find("tfoot").html('');
    $("#datatables-products-provider-clients-table-footer").find("tfoot").append(`
        <tr>
            <td colspan="4" class="text-right">${translations.payments_delivery_credits}:</td>
            <td>${window.formaterCurrency(totalCredit)} <a href="${urlDeliveriesCredits.replace(':date', date).replace(':user_id', userId)}" target="_blank">${translations.see_deliveries}</a></td>
        </tr>
        <tr>
            <td colspan="4" class="text-right">${translations.total_delivery_normal}:</td>
            <td>${window.formaterCurrency(totalNormal)}</td>
        </tr>
    `);
    let total = 0;
    response.products.map((product) => {
        total+= (response.item ? parseFloat(product.pivot.price) : product.total);
    });
    totalPayments = totalCredit;
    total+= totalPayments;
               
    $("#datatables-products-provider-clients-table-footer").find("tfoot").append(`
        <tr>
            <th colspan="4" class="text-right">${translations.total_deliveries_to_day}:</th>
            <th>${window.formaterCurrency(total)}</th>
        </tr>
        <tr>
            <th colspan="4" class="text-right">${translations.total_delivered_provider}:</th>
            <th><input type="${(response.item ? 'text' : 'number')}" ${(response.item ? 'disabled' : '')} class="form-control" id="input-total-delivered-provider" value="${(totalProvider == 0 ? totalProvider : window.formaterCurrency(totalProvider))}"></th>
        </tr>
    `);  
}

window.hideAndShow = (id) => {
    const element = $(`#${id}`).find('tbody');
    if ($(element).is(":visible"))
        $(element).hide(400);
    else
        $(element).show(400);
}