import Swal from 'sweetalert2';
const translations = JSON.parse($("meta[name=translations]").attr("content"));
let show = true;
let products = [];
const urlProductsSold = $("meta[name=url-products-sold]").attr("content");
$(() => {
    $("#file").hide();
    $("#file").change(()=>{
        var archivo = $("#file").val();
        var extensiones = archivo.substring(archivo.lastIndexOf("."));
        if(extensiones != ".xlsx" ){
            var error = "El archivo de tipo " + extensiones + " no es válido";
            Swal.fire('Error',error,'error'); 
            location.reload();
        }else{
            $("#form-file").submit();
        }
      
    });

    $("#uploadbtn").click((e)=>{
        $("#file").click();    
    });

    $(".input-number").keydown(function(event) {
        if(event.shiftKey)
        {
             event.preventDefault();
        }
 
        if (event.keyCode == 46 || event.keyCode == 8)    {
        }
        else {
             if (event.keyCode < 95) {
               if (event.keyCode < 48 || event.keyCode > 57) {
                     event.preventDefault();
               }
             } 
             else {
                   if (event.keyCode < 96 || event.keyCode > 105) {
                       event.preventDefault();
                   }
             }
           }
    });
    
    $("#btn-add-product-modal").off("click");
    $("#btn-add-product-modal").on("click", (e) => {
        if (show) {
            window.initDatatables();
            show = false;   
        }       
        //deleteRow(); 
    });    
    $("#btn-accpet-products").off('click');
    $("#btn-accpet-products").on('click', (e) => {
        if (products.length > 0) {                   
            updateTable(products);
            $('#product-modal').modal('hide');
        } else {
            window.getMessage(translations.select_product);        
        }                
    });
});

window.addProduct = (e) => {
    const item = $(e).data('item');
    const product = products.find((e) => e.id == item.id);
    if (!product && $(e).prop('checked'))
        products.push(item);
    else {        
        products = products.filter(product => product.id !== item.id)
    }
}

const updateTable = (products) => {    
    let productsSelect = [];
    products.map(product => productsSelect.push({id: product.id}));
    window.loading(translations.loading);
    window.sendRequest({
        url: urlProductsSold,
        data: {products: productsSelect, date_from: $("#date_from").val(), date_to: $("#date_to").val(), type: $("#type").val(), status: $("#status").val()},
        success: (response) => {
            if (response.error)
                window.getMessage(response.message);
            else {
                $(`#datatables-products-filters-table`).find("tbody").html('');
                let data = [];
                response.products.map((product) => data.push([product.name, product.category.name, product.unit.name, product.sold, window.formaterCurrency(product.total)]));                
                window.initDatatablesWithOutAjax($(`#datatables-products-filters-table`), data);                    
                Swal.clickCancel();  
            }                
        },
        error: (e) => {
            console.log("Error", e);
            Swal.clickCancel();
        }
    }, {processData: true});        
}