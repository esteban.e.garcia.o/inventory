const routeSupply = $("meta[name=load-supply]").attr("content");
const deliveriesCredit = $("meta[name=deliveries-credit]").attr("content");

$(document).ready(() => {  
  $('.collapse').collapse();
  $('#accordion').collapse({
    toggle: false
  })
  productsSupply(routeSupply);
  deliveriesCredits(deliveriesCredit);
});

function productsSupply(url) {
  $.get(url, function (data) {
    $("#countSupply").html(data.length);
    if (!$.isEmptyObject(data)) {
      //let x= 0;
      var html = ``;
      $.each(data, function (i, item) {
        html += `<div class="col mb-2 py-2">
                    <div class="card ">
                      <div class="card-body">
                        <h4 class="card-title">${item.name}</h4>
                        <p class="card-text">${item.description}</p>
                      </div>
                      <div class="card-footer">
                        <h3 class="card-text text-danger">Existencia: ${item.stock}</h3>
                      </div>
                    </div>
                </div> `;

      })
      html += ``;
      $("#load-supply").html(html);
    } else {
      html = `<h1 class="text-center text-primary">Aun tienes productos para surtir</h1>`;
      $("#load-supply").html(html);
    }

  });
}

function deliveriesCredits(url) {
  $.get(url, function (data) {   
    console.log(data); 
    $("#countDelivery").html(data.length);

    $.each(data, function (i, item) {
      $('#deliveries-credit').append(`
      <div class="col mb-2 py-2">
        <div class="card ">
          <div class="card-body">
            <h4 class="card-title">Cliente: ${item.client.longname}</h4>
            <p class="card-text">Total: ${window.formaterCurrency(item.total)}</p>
          </div>
          <div class="card-footer">
            <h3 class="card-text text-danger">Adeuda: ${window.formaterCurrency(item.should)}</h3>
          </div>
        </div>
      </div> `);
    });
  });
}



