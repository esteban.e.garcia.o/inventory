require('./bootstrap');
import Swal from 'sweetalert2';
import assign from 'lodash-es/assign';

$(document).ready(function(){
    setInterval(() => {
        $('[data-toggle="tooltip"]').tooltip();
    }, 1000);
});

window.loading = (title) => {
    Swal.fire({
        title,
        html: '',
        timer: false,
        timerProgressBar: false,
        onBeforeOpen: () => {
          Swal.showLoading();
        },
        onClose: () => {
          
        }
      }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
          console.log('I was closed by the timer')
        }
      });
}
(function($){
  if (typeof sendRequest === "undefined") {
    window.sendRequest = function(data, options = {}){
      var method = data.method ? data.method : 'POST';      
      const config = {
        method,
        url: data.url,
        dataType: 'json',
        data: data.data,
        processData: false,
        headers: {"Authorization": "Bearer " + $('meta[name=api-token]').attr('content')},
        success: data.success,
        beforeSend: data.beforeSend,
        complete: data.complete,
        error: function(xhr, status, error){
          //Callback from fail
          if(data.error){data.error(xhr);}
        }
      }
      return $.ajax(assign(config, options));
    };
  }
})($);

window.getMessage = (title, icon = 'error', callback = null) => {
  Swal.fire({
      icon,
      title,
      text: '',
  }).then(callback);
}

window.formaterCurrency = (price) => {
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 0
  });
  return formatter.format(price);
}