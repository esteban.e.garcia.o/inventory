const urlDatatable = $("meta[name=url-datatables]").attr("content");
const active = $("meta[name=active-datatables]").attr("content");
$(() => {
    if (active === "1") {
        window.initDatatables();        
    }    
});

window.initDatatables = () => {
    $('table[data-table="true"]').each((key, item) => {
        const columns = [];
        $(item).find("th[data-header]").each((k, i) => {
            const header = $(i).data('header');
            columns.push({data: header, name: header, 'defaultContent': ''});
        });
        $(item).DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: urlDatatable,
                beforeSend: function (request) {
                    let token = $('meta[name=api-token]').attr('content');
                    request.setRequestHeader("Authorization", `Bearer ${token}`);
                },
            },
            "columns": columns,
            "language": {
                url: "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
            },
            drawCallback:  (e) => {                                
                if(typeof window.drawCallback === "function")
                   window.drawCallback(e);    
            }
        });
    });
}

window.initDatatablesWithOutAjax = (item, data) => {    
    if (!$.fn.DataTable.isDataTable( item )) {
        $(item).DataTable({
            data,
            "language": {
                url: "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
            },
        });
    } else {
        let table = $(item).DataTable();
        table.destroy();        
        window.initDatatablesWithOutAjax(item, data);
    }                   
}