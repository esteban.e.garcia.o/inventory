
$(() => {    
    
});

window.seeInputs = (e) => {    
    const inputs = $(e).data('items');    
    updateTable(inputs);    
    $("#materials-inputs-modal").modal('show');    
}

const updateTable = (inputs) => {
    $("#materials-inputs-table").find("tbody").html('');
    inputs.map((input) => {        
        $("#materials-inputs-table").find("tbody").append(`
            <tr>
                <td>${input.manager.longname}</td>
                <td>${input.created}</td>
                <td>${input.quantity}</td>
            </tr>
        `);
    }); 
}