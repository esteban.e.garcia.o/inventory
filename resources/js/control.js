import Swal from 'sweetalert2';
let show = true;
let products = [];
const translations = JSON.parse($("meta[name=translations]").attr("content"));
const type = $("meta[name=type]").attr("content");
const urlStoreControls = $("meta[name=url-store-controls]").attr("content");
const urlProviderSignature = $("meta[name=url-provider-signature]").attr("content");
const selected = '<p>&#x2713; ' + translations.selected + '</p>';
$(() => {    
    $("#btn-add-product-modal").off("click");
    $("#btn-add-product-modal").on("click", (e) => {
        if (show) {
            window.initDatatables();
            show = false;   
        }       
        deleteRow(); 
    });

    $("#btn-accpet-products").off('click');
    $("#btn-accpet-products").on('click', (e) => {
        if (products.length > 0) {            
            updateTable(products);
            $('#product-modal').modal('hide');
        } else {
            window.getMessage(translations.select_product);        
        }                
    });

    $("#btn-controls-save").off('click');
    $("#btn-controls-save").on('click', (e) => {
        if (products.length > 0) {
            const productsTemp = products.filter((product) => product.quantity === undefined);
            if (0 == productsTemp.length) {
                const productsMin = [];
                products.map((product) => productsMin.push({id: product.id, stock: parseInt(product.stock), quantity: parseInt(product.quantity)}));
                let data = {products: productsMin, user_id: $("#user_id").val(), material_id: $("#material_id").val()};
                if (type == "O") {
                    confirmSignature(data);
                } else 
                    sendControls(data);
            } else 
                window.getMessage(translations.necessary_quantity);            
        } else
            window.getMessage(translations.have_not_prodcuts);        
    });   
    
    $(document).on('focusout', '.quantity',(e) => {
        e = e.currentTarget;
        const id = $(e).data('id');
        let quantity = undefined;
        if ($(e).val() !== "") {
            quantity = parseInt($(e).val());
        } else {
            window.getMessage(translations.only_numbers);
        }        
        products.map((product) => {
            if (product.id == id) {
                if (quantity === undefined) {
                    product.quantity = quantity;
                } else {
                    if (quantity <= product.stock || type == "I")
                        product.quantity = quantity;
                    else {
                        window.getMessage(translations.quantity_exceeds.replace(':stock', product.stock).replace(':quantity', quantity));
                        $(e).val('');
                    }                        
                }
            }
        });                
    });
});

const sendControls = (data) => {
    window.loading(translations.loading);
    window.sendRequest({
        url: urlStoreControls,
        data: data,
        success: (response) => {
            if (response.error)
                window.getMessage(response.message);
            else
                window.getMessage(response.message, 'success', () => window.location.href = $("#btn-cancel-controls").attr('href') + '?action=ok');                                                                                
        },
        error: (e) => {
            console.log("Error", e);
            Swal.clickCancel();
        }
    }, {processData: true});
} 

window.seeProducts = (e) => {    
    const products = $(e).data('items');    
    updateProductsControls(products);
    $("#product-controls-modal").modal('show');    
}

const updateProductsControls = (products) => {
    $("#controls-products-table").find("tbody").html('');
    products.map((product) => {        
        $("#controls-products-table").find("tbody").append(`
            <tr>
                <td>${product.name}</td>
                <td>${product.stock}</td>
                <td>${product.category.name}</td>
                <td>${product.unit.name}</td>
                <td>${product.pivot.quantity}</td>
            </tr>
        `);
    }); 
}

const updateTable = (products) => {
    let control = (type == "I" ? 'inputs' : 'outputs');
    $(`#datatables-${control}-table`).find("tbody").html('');
    products.map((product) => {
        $(`#datatables-${control}-table`).find("tbody").append(`
            <tr>
                <td>${product.name}</td>
                <td>${product.stock}</td>
                <td>${product.category.name}</td>
                <td>${product.unit.name}</td>
                <td>
                    <input type="number" data-id="${product.id}" class="form-control quantity" placeholder="${translations.quantity}" value="">
                </td>
                <td>
                    <a href="#" onclick="window.deleteProduct(this, '${product.id}')" class="btn btn-outline-dark" data-toggle="tooltip" data-placement="top" title="${translations.delete}"><i class="far fa-trash-alt"></i></a>
                </td>
            </tr>
        `);
    });    
}

window.deleteProduct = (e, id) => {
    $(e).parents('tr').remove();
    products = products.filter((product) => product.id !== id);
    const input = $("#datatables-products-table").find('input[data-id="' + id +'"]');
    $(input).siblings("p").remove();
    $(input).prop("checked", false);
    $(input).show();
}

window.addProduct = (e) => {
    const item = $(e).data('item');
    const product = products.find((e) => e.id == item.id);
    if (!product)
        products.push(item);
    $(e).parent().append(selected);
    $(e).hide();
}

window.drawCallback = (datatable) => {
    setTimeout(() => {
       deleteRow(); 
    }, 1500);
}

const deleteRow = () => {
    $.each(products, (key, item) => {
        const input = $("#datatables-products-table").find('input[data-id="' + item.id +'"]');
        $(input).siblings("p").remove();
        $(input).parent().append(selected);
        $(input).hide();
    });
}

const confirmSignature = (data) => {
    Swal.fire({
        title: 'Se requiere confirmación del proveedor',
        input: 'password',
        inputAttributes: {
          autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Enviar',
        showLoaderOnConfirm: true,
        preConfirm: (login) => {
            if (login == "") {
                Swal.showValidationMessage(
                    'Ingresa una contraseña'
                );
            } else {
                return window.sendRequest({
                    url: urlProviderSignature.replace(':id', data.user_id),
                    data: {password: login},
                    success: (response) => {
                        if (response.error) {
                            Swal.showValidationMessage(
                                response.message
                            );
                        }
                        return response;
                    },
                    error: (e) => {
                        console.log("Error", e);
                        Swal.clickCancel();
                    }
                }, {processData: true});
            }
        },
        allowOutsideClick: () => !Swal.isLoading()
      }).then((result) => {
        if (result.value) {
          Swal.fire({
            title: 'Contraseña correcta',
          }).then(() => {
            sendControls(data);
          });
        }
    });
}