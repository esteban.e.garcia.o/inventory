<table data-table="true" id="datatables-{{ $resource }}-table" class="table table-hover table-condensed">
    <thead>
    <tr>
        @foreach ($headers as $key => $header)
            <th data-header="{{ $key }}">{{ $header }}</th>
        @endforeach
    </tr>
    </thead>
</table>
@push('metas')
    <meta name="url-datatables" content="{{ $url }}">
    <meta name="active-datatables" content="{{ isset($active) ? $active : '1' }}">
@endpush
@push('scripts')
    <script src="{{ asset('js/build/table.js') }}" defer></script>   
@endpush