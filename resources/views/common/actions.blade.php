@if (isset($edit) && $edit == true)
    <a href="{{ $routeEdit }}" class="btn btn-outline-dark" data-toggle="tooltip" data-placement="top" title="@lang('general.edit')"><i class="fas fa-pencil-alt"></i></a>
@endif

@if (isset($check) && $check == true) 
    <div class="form-group form-check">
        <input data-id="{{ $item->id }}" data-item="{{ $item->toJson() }}" type="checkbox" class="form-check-input" onclick="{{$checkEvent}}">
    </div>    
@endif

@if (isset($see) && $see == true)
    <button data-items="{{ isset($items) ? $items->toJson(): '[]' }}" onclick="{{ $seeEvent }}" class="btn btn-outline-dark" data-toggle="tooltip" data-placement="top" title="{{ $title }}"><i class="far fa-eye"></i></button>
@endif

@if (isset($credit) && $credit == true)
    <button data-items="{{ isset($itemsCredit) ? $itemsCredit->toJson(): '[]' }}" onclick="{{ $creditEvent }}" class="btn btn-outline-dark" data-toggle="tooltip" data-placement="top" title="@lang('delivery.see_credit')"><i class="far fa-credit-card"></i></button>
@endif