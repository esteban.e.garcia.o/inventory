<i 
    style="cursor:pointer;" 
    class="far fa-minus-square float-right" 
    data-toggle="tooltip" 
    data-placement="top" 
    title="@lang('delivery.hide_and_show')"
    onclick="{{ $event }}"
></i>