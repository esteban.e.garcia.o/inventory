<div class="{{ isset($clas) ? $clas : 'col-md-12' }}">
    <div class="form-group">
        @isset($label)
            <label for="{{ $id }}">{{ $label }}</label>        
        @endisset
        <select 
            class="form-control @error($id) is-invalid @enderror" 
            id="{{ $id }}" 
            name="{{ $id }}"
            value="{{ $value }}">
            @php
                if (is_null($value))
                    $value = "1";
                $index = 0;
            @endphp
            @foreach ($options as $key => $option) 
                @if(isset($default) && $index == 0)
                    <option disabled selected>{{ $default }}</option>
                @endif               
                <option value="{{ $key }}" @if ($key == $value) selected @endif >{{ $option }}</option>
                @php
                    $index++;
                @endphp
            @endforeach
        </select>
        @error($id)
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>