<div class="{{ isset($clas) ? $clas : 'col-md-12' }}">
    <div class="form-group">
        <label for="{{ $id }}">{{ $label }}</label>
        <input 
            type="{{ isset($type) ? $type : 'text' }}" 
            class="form-control @error($id) is-invalid @enderror"
            disabled
            id="{{ $id }}" 
            name="{{ $id }}" 
            @if (isset($required) && $required == true) required @endif
            placeholder="@isset($placeholder) {{ $placeholder }} @else {{ $label }} @endisset"
            value="{{ $value }}"
            >
        
    </div>
</div>