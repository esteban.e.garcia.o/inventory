@extends('layouts.app')

@section('content')
    @parent
    @include('admin.controls.index', ['type' => 'outputs', "headers" => [
        "created_by" => __('control.created_by'),
        "created_at" => __('general.datatables.created_at'),
        "user_id" => __('control.provider'),
        "actions" => __('general.datatables.actions'),
    ]])
@endsection