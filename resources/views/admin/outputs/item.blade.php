@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">{{ __('general.' . (isset($item) ? 'edit' : 'add')) }} @lang('control.output')</x-slot>
        @include('admin.controls.content', ['type' => 'outputs'])
    </x-content>
@endsection
@push('metas')
    <meta name="translations" content="{{ json_encode(__('control')) }}">
    <meta name="type" content="O">
    <meta name="url-store-controls" content="{{ route('api.outputs.store') }}">
    <meta name="url-provider-signature" content="{{ route('api.outputs.provider.signature', ['id' => ':id']) }}">    
@endpush
@push('scripts')
    <script src="{{ asset('js/build/control.js') }}" defer></script>   
@endpush