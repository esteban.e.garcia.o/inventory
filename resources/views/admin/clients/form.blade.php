<div class="row">
    @include('common.input', [
        "id" => "longname",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.longname'),
        "value" => (isset($item->longname) ? $item->longname : old('longname'))
    ])
    @include('common.input', [
        "id" => "direction",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.direction'),
        "value" => (isset($item->direction) ? $item->direction : old('direction'))
    ])
    
</div>
<div class="row">
    @include('common.input', [
        "id" => "city",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.city'),
        "value" => (isset($item->city) ? $item->city : old('city'))
    ])
    @include('common.input', [
        "id" => "phone",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.phone'),
        "value" => (isset($item->phone) ? $item->phone : old('phone'))
    ])
    
</div>
<div class="row">
    @include('common.input', [
        "id" => "name_store",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.name_store'),
        "value" => (isset($item->name_store) ? $item->name_store : old('name_store'))
    ])
    
    @include('common.select', [
        "id" => "status",
        "clas" => "col-md-6",
        "label" => __('general.datatables.status'),
        "options" => [
            "1" => __('general.active'),
            "0" => __('general.inactive'),
        ],
        "value" => (isset($item->status) ? $item->status : old('status'))
    ])
    
    
</div>
<div class="row">
    @if(isset($item) && !isset($item->location))
        @include('common.label', [
            "id" => "location",
            "type"=>"text",
            "clas" => "col-md-6",
            "label" => __('general.location'),
            "value" => __('general.hasnolocation',["name_store"=>$item->name_store])
        ])
    @endif
   
</div>