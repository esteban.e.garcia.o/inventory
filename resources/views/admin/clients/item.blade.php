@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">{{ __('general.' . (isset($item) ? 'edit' : 'add')) }} @lang('general.client')</x-slot>
        <div class="row py-4">
            <div class="col-md-12">
                @component('components.form', [
                    "item" => (isset($item) ? $item : null),
                    "resource" => "clients",
                    "back" => route('admin.clients.index'),
                    "updateResource" => "client"
                ])
                @include('admin.clients.form', ['item' => (isset($item) ? $item : null)])
                @endcomponent
            </div>
        </div>        
    </x-content>
@endsection