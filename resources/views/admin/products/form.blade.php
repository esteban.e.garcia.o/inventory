<div class="row">
    @include('common.input', [
        "id" => "code",
        "required" => true,
        "clas" => "col-md-6 input-number",
        "label" => __('general.datatables.code'),
        "value" => (isset($item->code) ? $item->code : old('code')),
        "type" => "number"
    ])
    @include('common.input', [
        "id" => "name",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.name'),
        "value" => (isset($item->name) ? $item->name : old('name'))
    ])
</div>
<div class="row">
    @include('common.input', [
        "id" => "price",
        "type"=>"number",
        "required" => true,
        "clas" => "col-md-4 input-number",
        "label" => __('general.datatables.price'),
        "value" => (isset($item->price) ? $item->price : old('price'))
    ])
    @include('common.input', [
        "id" => "stock",
        "type"=>"number",
        "required" => true,
        "clas" => "col-md-4",
        "label" => __('general.datatables.stock'),
        "value" => (isset($item->stock) ? $item->stock : old('stock'))
    ])
    @include('common.input', [
        "id" => "kilos",
        "clas" => "col-md-4 input-number",
        "label" => __('general.kilos'),
        "value" => (isset($item->kilos) ? $item->kilos : old('total')),
        "type" => "number"
    ])
</div>
<div class="row">
    @include('common.input', [
        "id" => "min_stock",
        "type"=>"number",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.datatables.min_stock'),
        "value" => (isset($item->min_stock) ? $item->min_stock : old('min_stock'))
    ])

    @include('common.input', [
        "id" => "barcode",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.datatables.barcode'),
        "value" => (isset($item->barcode) ? $item->barcode : old('barcode'))
    ])    
</div>
<div class="row">
    @include('common.input', [
        "id" => "description",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.datatables.description'),
        "value" => (isset($item->description) ? $item->description : old('description'))
    ])

    @include('common.select', [
        "id" => "status",
        "clas" => "col-md-6",
        "label" => __('general.datatables.status'),
        "options" => [
            "1" => __('general.active'),
            "0" => __('general.inactive'),
        ],
        "value" => (isset($item->status) ? $item->status : old('status'))
    ])
</div>
<div class="row">
    @include('common.select', [
        "id" => "category_id",
        "clas" => "col-md-6",
        "label" => __('general.category'),
        "options" => $categories,
        "value" => (isset($item->category_id) ? $item->category_id : old('category_id'))
    ])

    @include('common.select', [
        "id" => "unit_id",
        "clas" => "col-md-6",
        "label" => __('general.unit'),
        "options" => $units,
        "value" => (isset($item->unit_) ? $item->unit_id : old('unit_id'))
    ])
</div>