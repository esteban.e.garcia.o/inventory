@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('general.administration', ['model' => __('navigation.menu.products')])</x-slot>
        <div class="row py-4">
            <div class="col-md-12">
                <a href="{{ route('admin.products.sold.filter') }}" class="btn btn-outline-primary float-right">@lang('general.sold_products')</a>
                <a href="{{ route('admin.products.create') }}" class="btn btn-outline-primary float-right"><i class="fas fa-plus"></i> @lang('general.add')</a>                
                <a href="{{ route('home') }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('general.cancel')</a>
                <a href="{{ route('admin.products.export') }}" class="btn btn-outline-success"><i class="fas fa-download"></i> @lang('general.download')</a>
                <button class="btn btn-outline-primary" id="uploadbtn"><i class="fas fa-unload"></i> @lang('general.up')</button>
                <form id="form-file" action="{{route('admin.products.import')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="file"  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" id="file" name="file" style="display:none" required>
                </form>
                @error("file")
                        <span class="" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span>
                @enderror
                
               
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('common.table', [
                    "resource" => "products",
                    "headers" => [
                        "code" => __('general.datatables.code'),
                        "name" => __('general.name'),
                        "stock" => __('general.datatables.stock'),
                        "price" => __('general.datatables.price'),
                        "category" => __('general.category'),
                        "unit" => __('general.unit'),
                        "status" => __('general.datatables.status'),
                        "created_at" => __('general.datatables.created_at'),
                        "actions" => __('general.datatables.actions'),
                    ],
                    "url" => route('api.products.datatables')
                ])
            </div>
        </div>
    </x-content>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('js/build/products.js') }}"></script>
@endpush