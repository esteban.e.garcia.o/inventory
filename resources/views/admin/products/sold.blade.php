@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('general.filter') @lang('general.sold_products')</x-slot>
        
        <div class="row py-4">
            <div class="col-md-8">
                <h4>@lang('general.list_products_filters')</h4>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <button type="button" id="btn-add-product-modal" data-toggle="modal" data-target="#product-modal" class="btn btn-outline-primary float-right"><i class="fas fa-plus"></i> @lang('general.filter') @lang('general.products')</button>
                </div>                
            </div>
        </div>  
        <div class="row">
            <div class="col-md-12">
                @include('admin.controls.table', ['id' => "datatables-products-filters-table", 'withoutAjax' => true, 'headers' => [
                    __('general.name'),                    
                    __('general.category'),
                    __('general.unit'),                    
                    __('general.sold'),
                    __('general.total')
                ]])    
            </div>    
        </div>      
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">                    
                    <a href="{{ route("admin.products.index") }}" id="btn-cancel-controls" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('general.cancel')</a>
                </div>
            </div>
        </div>
        @include('admin.controls.modal', ['dates' => true])
    </x-content>
@endsection
@push('metas')
    <meta name="translations" content="{{ json_encode(__('control')) }}">
    <meta name="url-products-sold" content="{{ route('api.products.sold.filter') }}">    
@endpush
@push('scripts')
    <script src="{{ asset('js/build/products.js') }}" defer></script>   
@endpush