@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">{{ __('general.' . (isset($item) ? 'edit' : 'add')) }} @lang('general.product')</x-slot>
        <div class="row py-4">
            <div class="col-md-12">
                @component('components.form', [
                    "item" => (isset($item) ? $item : null),
                    "resource" => "products",
                    "back" => route('admin.products.index'),
                    "updateResource" => "product"
                ])
                @include('admin.products.form', ['item' => (isset($item) ? $item : null)])
                @endcomponent
            </div>
        </div>        
    </x-content>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('js/build/products.js') }}"></script>
@endpush