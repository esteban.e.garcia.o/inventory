@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('general.administration', ['model' => __('navigation.menu.cashcuts')])</x-slot>
        @include('admin.general.search_provider', ['providers' => $providers, 'idBtn' => 'btn-search-provider', 'date' => true])        
        @include('admin.general.products_deliveries')
        <div class="row py-4">
            <div class="col-md-12">                
                <a href="{{ route('home') }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('general.cancel')</a>
            </div>
        </div>        
    </x-content>
@endsection
@push('metas')
    <meta name="translations" content="{{ json_encode(array_merge(__('delivery'), __('control'))) }}">
    <meta name="url-products-cashcuts" content="{{ route('api.cashcuts.products', ['user_id' => ':user_id']) }}">
    <meta name="url-deliveries-credits" content="{{ route('admin.deliveries.credit.index') }}">    
@endpush
@push('scripts')
    <script src="{{ asset('js/build/table.js') }}" defer></script>   
    <script src="{{ asset('js/build/deliveries.js') }}" defer></script>   
@endpush