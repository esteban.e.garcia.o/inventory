<div class="row">
    @include('common.input', [
        "id" => "code",
        "required" => true,
        "clas" => "col-md-6 input-number",
        "label" => __('general.num_bill'),
        "value" => (isset($item->code) ? $item->code : old('code')),
        "type" => "number"
    ])
    @include('common.input', [
        "id" => "name",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.name'),
        "value" => (isset($item->name) ? $item->name : old('name'))
    ])    
</div>
<div class="row">
    @include('common.input', [
        "id" => "total",
        "required" => true,
        "clas" => "col-md-6 input-number",
        "label" => __('general.kilos'),
        "value" => (isset($item->total) ? $item->total : old('total')),
        "type" => "number"
    ])
    @include('common.select', [
        "id" => "status",
        "clas" => "col-md-6",
        "label" => __('general.datatables.status'),
        "options" => [
            "1" => __('general.active'),
            "0" => __('general.inactive'),
        ],
        "value" => (isset($item->status) ? $item->status : old('status'))
    ])
</div>
