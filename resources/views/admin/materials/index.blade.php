@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('general.administration', ['model' => __('navigation.menu.materials')])</x-slot>
        <div class="row py-4">
            <div class="col-md-12">
                <a href="{{ route('admin.materials.create') }}" class="btn btn-outline-primary float-right"><i class="fas fa-plus"></i> @lang('general.add')</a>
                <a href="{{ route('home') }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('general.cancel')</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('common.table', [
                    "resource" => "materials",
                    "headers" => [
                        "code" => __('general.num_bill'),
                        "name" => __('general.name'),
                        "total" => __('general.kilos'),
                        "remaining" => __('general.remaining'),
                        "bales" => __('general.bales'),                                                
                        "actions" => __('general.datatables.actions'),
                    ],
                    "url" => route('api.materials.datatables')
                ])
            </div>
        </div>
        @component('components.modal', ['id' => 'materials-inputs-modal', 'title' => __('navigation.menu.inputs')])
            <div class="row">
                <div class="col-md-12">                    
                    @include('admin.controls.table', ['id' => 'materials-inputs-table', 'headers' => [
                        __('control.created_by'),
                        __('general.datatables.created_at'),
                        __('general.bales'),
                    ]])
                </div>
            </div>
            @slot('slotButton')@endslot
        @endcomponent
    </x-content>
@endsection
@push('scripts')
    <script src="{{ asset('js/build/materials.js') }}" defer></script>   
@endpush