@component('components.modal', ['id' => 'product-modal', 'title' => __('navigation.menu.products')])
    @isset($dates)
        <div class="row">
            @include('common.input', [
                "id" => "date_from",
                "required" => false,
                "clas" => "col-md-3",
                "value" => date('Y-m-d'),
                "type" => "date",
                "label" => __('general.date_from')
            ])  
            @include('common.input', [
                "id" => "date_to",
                "required" => false,
                "clas" => "col-md-3",
                "value" => date('Y-m-d'),
                "type" => "date",
                "label" => __('general.date_to')
            ]) 
            @include('common.select', [
                "id" => "type",
                "clas" => "col-md-3",
                "label" => __('general.type'),
                "options" => ['N' => __('navigation.menu.normal'), 'C' => __('navigation.menu.credit')],    
                "value" => null            
            ])
            @include('common.select', [
                "id" => "status",
                "clas" => "col-md-3",
                "label" => __('general.datatables.status'),
                "options" => ['F' => __('delivery.finish'), 'P' => __('delivery.pending')],    
                "value" => null            
            ])  
        </div>
    @endisset
    <div class="row">
        <div class="col-md-12">
            @include('common.table', [
                "resource" => "products",
                "headers" => [
                    "name" => __('general.name'),
                    "stock" => __('general.datatables.stock'),                    
                    "category" => __('general.category'),
                    "unit" => __('general.unit'),                    
                    "actions" => __('control.select'),
                ],
                "url" => route('api.products.modal.datatables') . (isset($is_materials) ? '?materials=true' : ''),
                "active" => false
            ])
            @slot('slotButton')
                <button type="button" id="btn-accpet-products" class="btn btn-outline-primary">@lang('control.accept')</button>
            @endslot
        </div>
    </div>
@endcomponent