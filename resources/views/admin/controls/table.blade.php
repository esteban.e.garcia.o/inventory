<table id="{{ $id }}" class="table table-hover table-condensed" data-table-without-ajax="{{ isset($withoutAjax) ? 'true' : 'false' }}">
    <thead>
        <tr>
            @foreach ($headers as $header)
                <th>{{ $header }}</th>    
            @endforeach            
        </tr>
    </thead>
    <tbody></tbody>
    <tfoot></tfoot>
</table>