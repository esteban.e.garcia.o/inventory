<x-content>
    <x-slot name="title">@lang('general.administration', ['model' => __("navigation.menu.$type")])</x-slot>
    <div class="row py-4">
        <div class="col-md-12">
            <a href="{{ route("admin.$type.create") }}" class="btn btn-outline-primary float-right"><i class="fas fa-plus"></i> @lang('general.add')</a>
            @if ($type == "inputs")
                <a href="{{ route("admin.$type.create", ['materials' => true]) }}" class="btn btn-outline-primary float-right"><i class="fas fa-plus"></i> @lang('general.add') con @lang('navigation.menu.materials')</a>
            @endif
            <a href="{{ route('home') }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('general.cancel')</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('common.table', [
                "resource" => $type,
                "headers" => $headers,
                "url" => route("api.$type.datatables")
            ])
        </div>
    </div>
    @component('components.modal', ['id' => 'product-controls-modal', 'title' => __('navigation.menu.products')])
        <div class="row">
            <div class="col-md-12">                    
                @include('admin.controls.table', ['id' => 'controls-products-table', 'headers' => [
                    __('general.name'),
                    __('general.datatables.stock'),
                    __('general.category'),
                    __('general.unit'),
                    __('control.quantity'),
                ]])
            </div>
        </div>
        @slot('slotButton')@endslot
    @endcomponent
</x-content>
@push('metas')
    <meta name="translations" content="{{ json_encode(__('control')) }}">
@endpush
@push('scripts')
    <script src="{{ asset('js/build/control.js') }}" defer></script>   
@endpush