<div class="row py-4">
    <div class="col-md-8">
        <h4>@lang('control.list_products_add')</h4>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <button type="button" id="btn-add-product-modal" data-toggle="modal" data-target="#product-modal" class="btn btn-outline-primary float-right"><i class="fas fa-plus"></i> @lang('general.add') @lang('general.products')</button>
        </div>                
    </div>
</div>  
@isset($providers)
    <div class="row">
        @include('common.select', [
            "id" => "user_id",
            "clas" => "col-md-4",
            "label" => __('control.select_provider'),
            "options" => $providers,
            "value" => ''
        ])
        <div class="col-md-8">
            
        </div>
    </div>
@endisset
@isset($is_materials)
    <div class="row">
        @include('common.select', [
            "id" => "material_id",
            "clas" => "col-md-4",
            "label" => __('control.select_material'),
            "options" => $materials,
            "value" => ''
        ])
        <div class="col-md-8">
            
        </div>
    </div>
@endisset
<div class="row">
    <div class="col-md-12">
        @include('admin.controls.table', ['id' => "datatables-$type-table", 'headers' => [
            __('general.name'),
            __('general.datatables.stock'),
            __('general.category'),
            __('general.unit'),
            __('control.quantity'),
            __('general.datatables.actions')
        ]])    
    </div>    
</div>      
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <button type="button" class="btn btn-outline-primary float-right" id="btn-controls-save"><i class="fas fa-save"></i> @lang('general.save')</button>
            <a href="{{ route("admin.$type.index") }}" id="btn-cancel-controls" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('general.cancel')</a>
        </div>
    </div>
</div>
@include('admin.controls.modal', ['materials' => (isset($is_materials) ? true : null)])