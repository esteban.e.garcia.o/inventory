@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('general.administration', ['model' => __("navigation.menu.$type")])</x-slot>
        <div class="row py-4">
            <div class="col-md-6">                
                <a href="{{ route('home') }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('general.cancel')</a>
            </div>
            <div class="col-md-6">
                @if (request('date'))
                    <div class="form-group">
                        <button type="button" id="btn-show-payments-modal" data-toggle="modal" data-target="#product-modal" class="btn btn-outline-primary float-right"> @lang('delivery.see_credit')</button>
                    </div> 
                @endif               
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @php
                    $headers = [
                        "provider" => __('control.provider'),
                        "client" => __('general.client'),                        
                        "total" => __('delivery.total'),                                                
                    ];
                    if ($type == "credit") {
                        $headers["initial_payment"] = __('delivery.initial_payment');
                        $headers["payment"] = __('delivery.payment');
                    }
                    $headers["status"] = __('general.datatables.status');
                    $headers["created"] = __('general.datatables.created_at');
                    $headers["actions"] = __('general.datatables.actions');
                @endphp
                @include('common.table', [
                    "resource" => "categories",
                    "headers" => $headers,
                    "url" => route('api.deliveries.payments.datatables') . "?type=$type" . (request('date') ? '&date=' . request('date') : '') . (request('user_id') ? '&user_id=' . request('user_id') : '')
                ])
            </div>
        </div>
        @component('components.modal', ['id' => 'products-credit-modal', 'title' => __('navigation.menu.products')])
            <div class="row">
                <div class="col-md-12">                    
                    @include('admin.controls.table', ['id' => 'credits-products-table', 'headers' => [
                        __('general.name'),
                        __('general.datatables.stock'),
                        __('general.category'),
                        __('general.unit'),
                        __('control.quantity'),
                    ]])
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <h4>@lang('delivery.total'): <b id="total-sum-products"></b></h4>
                </div>
            </div>
            @slot('slotButton')@endslot
        @endcomponent
        @component('components.modal', ['id' => 'credits-modal', 'title' => __('delivery.credits')])
            <div class="row">
                <div class="col-md-12">                    
                    @include('admin.controls.table', ['id' => 'credits-table', 'headers' => [
                        __('control.provider'),
                        __('delivery.payment'),
                        __('general.datatables.created_at')
                    ]])
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <h4>@lang('delivery.total'): <b id="total-sum-credits"></b></h4>
                </div>
            </div>
            @slot('slotButton')@endslot
        @endcomponent
        @component('components.modal', ['id' => 'credits-payments-modal', 'title' => __('general.payments')])
            <div class="row">
                <div class="col-md-12">                    
                    @include('admin.controls.table', ['id' => 'payments-table', 'headers' => [
                        __('general.client'),
                        __('delivery.payment'),
                        __('general.datatables.created_at')
                    ]])
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <h5>@lang('general.total_payments'): <b id="total-payments"></b></h5><br>
                    <h5>@lang('general.total_initial'): <b id="total-initial"></b></h5><br>
                    <h5>@lang('general.total'): <b id="total-general"></b></h5>                                        
                </div>
            </div>
            @slot('slotButton')@endslot
        @endcomponent
    </x-content>
@endsection
@push('metas')
    <meta name="translations" content="{{ json_encode(__('delivery')) }}">
    @if (request('date'))
        <meta name="url-payments" content="{{ route('api.payments.user.credit', ['user_id' => request('user_id'), 'date' => request('date')]) }}">    
    @endif
@endpush
@push('scripts')
    <script src="{{ asset('js/build/credits.js') }}" defer></script>   
@endpush