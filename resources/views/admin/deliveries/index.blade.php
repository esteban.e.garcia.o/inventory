@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('general.administration', ['model' => __('delivery.deliveries_of_day', ['date' => now()->isoFormat('dddd D'), 'month' => now()->isoFormat('MMMM')])])</x-slot>
        @include('admin.general.search_provider', ['providers' => $providers, 'idBtn' => 'btn-search-provider', 'date' => true])
        @include('admin.general.products_provider')
        @include('admin.general.products_deliveries')
        <div class="row py-4">
            <div class="col-md-12">
                <button data-url="{{ route('api.cashcuts.store') }}" class="btn btn-outline-primary float-right" id="btn-save-cashcuts"><i class="fas fa-save"></i> @lang('general.save')</button>
                <a href="{{ route('home') }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('general.cancel')</a>
            </div>
        </div>        
    </x-content>
@endsection
@push('metas')
    <meta name="translations" content="{{ json_encode(array_merge(__('delivery'), __('control'))) }}">
    <meta name="url-products-provider" content="{{ route('api.products.provider', ['user_id' => ':user_id']) }}">
    <meta name="url-deliveries-credits" content="{{ route('admin.deliveries.credit.index') }}">    
@endpush
@push('scripts')
    <script src="{{ asset('js/build/table.js') }}" defer></script>   
    <script src="{{ asset('js/build/deliveries.js') }}" defer></script>   
@endpush