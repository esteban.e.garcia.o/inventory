@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">{{ __('general.' . (isset($item) ? 'edit' : 'add')) }} @lang('control.input')</x-slot>
        @include('admin.controls.content', ['type' => 'inputs', 'is_materials' => (request('materials') ? request('materials') : null)])
    </x-content>
@endsection
@push('metas')
    <meta name="translations" content="{{ json_encode(__('control')) }}">
    <meta name="type" content="I">
    <meta name="url-store-controls" content="{{ route('api.inputs.store') }}">
@endpush
@push('scripts')
    <script src="{{ asset('js/build/control.js') }}" defer></script>   
@endpush