@extends('layouts.app')

@section('content')
    @parent
    @include('admin.controls.index', ['type' => 'inputs', "headers" => [
        "created_by" => __('control.created_by'),
        "created_at" => __('general.datatables.created_at'),
        "actions" => __('general.datatables.actions'),
    ]])
@endsection