@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
  <div class="col-md-12">
    <a class="float-right" href="{{ asset('app/inventory-app.apk') }}" target="_blank">Descargar App</a>
  </div>
  <div class="col-md-12 ">
    <div class="accordion" id="accordionExample">
      <div class="card ">
        <div class="card-header" id="headingOne">
          <h2 class="mb-0">
            
              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              {{ __('general.supplyinfo')}}  
              </button>
              <span class="countSupply badge badge-danger"><div id="countSupply">0</div>  </span>
              <a id="addProducts" href="{{ route('admin.inputs.create') }}" class="btn btn-outline-primary float-right"><i class="fas fa-plus"></i> @lang('general.inputs')</a><br>
          </h2> 
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
          <div class="card-body">
          
            <div class="card-columns">
              <div id="load-supply"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingTwo">
          <h2 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            {{ __('general.deliverOnCredit')}} 
            </button>
            <span class="countSupply badge badge-danger"><div id="countDelivery">0</div>  </span>
            <a id="addProducts" href="{{ route('admin.deliveries.credit.index') }}" class="btn btn-outline-primary float-right"><i class="fas fa-plus"></i> @lang('general.deliverOnCreditGo')</a><br>
          
          </h2>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
          <div class="card-body">
            <div class="card-columns">
              <div id="deliveries-credit"></div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>
  
@endsection
@push('metas')
  <meta name="load-supply" content="{{ route('admin.products.supply') }}">
  <meta name="deliveries-credit" content="{{ route('admin.deliveries.credit') }}">
@endpush
@push('scripts')
<script type="text/javascript" src="{{ asset('js/build/supply.js') }}"></script>
@endpush