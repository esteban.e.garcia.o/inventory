<div class="row">            
    <div class="col-md-11">
        <h4>@lang('delivery.products_delivery_clients')</h4>
    </div>            
    <div class="col-md-1">
        @include('common.hideShow', ['event' => "window.hideAndShow('datatables-products-provider-clients-table');"])
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @include('admin.controls.table', ['id' => "datatables-products-provider-clients-table", 'withoutAjax' => true, 'headers' => [
            __('general.name'),
            __('general.category'),
            __('general.unit'),
            __('delivery.delivered'),
            __('delivery.total'),
        ]])    
        <table id="datatables-products-provider-clients-table-footer" class="table table-hover table-condensed"><tfoot></tfoot></table>
    </div>                    
</div>
<div class="dropdown-divider"></div>