<div class="row py-4">
    @include('common.select', [
        "id" => "user_id",
        "clas" => "col-md-4",
        "default" => __('control.select_provider'),
        "options" => $providers,
        "value" => ''
    ])
    @if ($date == true)
        @include('common.input', [
            "id" => "date",
            "required" => false,
            "clas" => "col-md-4",
            "value" => date('Y-m-d'),
            "type" => "date"
        ])
    @endif
    <div class="col-md-{{ ($date == false ? '8' : '4') }}">
        <button id="{{ $idBtn }}" class="btn btn-outline-primary"><i class="fas fa-search"></i> @lang('delivery.search')</button>
    </div>
</div>