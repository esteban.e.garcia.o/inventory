<div class="row">            
    <div class="col-md-11">
        <h4>@lang('delivery.products_provider')</h4>
    </div>            
    <div class="col-md-1">
        @include('common.hideShow', ['event' => "window.hideAndShow('datatables-products-provider-table');"])
    </div>
</div>
<div class="row py-4">
    <div class="col-md-12">
        @include('admin.controls.table', ['id' => "datatables-products-provider-table", 'withoutAjax' => true, 'headers' => [
            __('general.name'),
            __('general.category'),
            __('general.unit'),                    
            __('delivery.current_quantity'),
            __('delivery.delivery_normal'),
            __('delivery.delivery_credit')
        ]])            
    </div>    
</div> 
<div class="dropdown-divider"></div>