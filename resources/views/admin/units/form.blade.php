<div class="row">
    @include('common.input', [
        "id" => "name",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.name'),
        "value" => (isset($item->name) ? $item->name : old('name'))
    ])
    @include('common.select', [
        "id" => "status",
        "clas" => "col-md-6",
        "label" => __('general.datatables.status'),
        "options" => [
            "1" => __('general.active'),
            "0" => __('general.inactive'),
        ],
        "value" => (isset($item->status) ? $item->status : old('status'))
    ])
    
</div>
