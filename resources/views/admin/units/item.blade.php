@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">{{ __('general.' . (isset($item) ? 'edit' : 'add')) }} @lang('general.unit')</x-slot>
        <div class="row py-4">
            <div class="col-md-12">
                @component('components.form', [
                    "item" => (isset($item) ? $item : null),
                    "resource" => "units",
                    "back" => route('admin.units.index'),
                    "updateResource" => "unit"
                ])
                @include('admin.units.form', ['item' => (isset($item) ? $item : null)])
                @endcomponent
            </div>
        </div>        
    </x-content>
@endsection