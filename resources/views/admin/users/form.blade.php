<div class="row">
    @include('common.input', [
        "id" => "user_name",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.datatables.user_name'),
        "value" => (isset($item->user_name) ? $item->user_name : old('user_name'))
    ])
    @include('common.input', [
        "id" => "name",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.name'),
        "value" => (isset($item->name) ? $item->name : old('name'))
    ])
</div>
<div class="row">
    @include('common.input', [
        "id" => "surnames",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.surnames'),
        "value" => (isset($item->surnames) ? $item->surnames : old('surnames'))
    ])
    @include('common.input', [
        "id" => "phone",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('general.datatables.phone'),
        "value" => (isset($item->phone) ? $item->phone : old('phone'))
    ])
</div>
<div class="row">
    @include('common.select', [
        "id" => "status",
        "clas" => "col-md-6",
        "label" => __('general.datatables.status'),
        "options" => [
            "1" => __('general.active'),
            "0" => __('general.inactive'),
        ],
        "value" => (isset($item->status) ? $item->status : old('status'))
    ])
    @include('common.select', [
        "id" => "role_id",
        "clas" => "col-md-6",
        "label" => __('general.datatables.roles'),
        "options" => $roles,
        "value" => (isset($item->roles) && $item->roles()->count() > 0) ? $item->roles()->first()->id : old('role_id')
    ])
</div>