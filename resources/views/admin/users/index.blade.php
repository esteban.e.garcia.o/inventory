@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('general.administration', ['model' => __('navigation.menu.users')])</x-slot>
        <div class="row py-4">
            <div class="col-md-12">
                <a href="{{ route('admin.users.create') }}" class="btn btn-outline-primary float-right"><i class="fas fa-plus"></i> @lang('general.add')</a>
                <a href="{{ route('home') }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('general.cancel')</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('common.table', [
                    "resource" => "users",
                    "headers" => [
                        "user_name" => __('general.datatables.user_name'),
                        "longname" => __('general.datatables.longname'),
                        "phone" => __('general.datatables.phone'),
                        "status" => __('general.datatables.status'),
                        "created_at" => __('general.datatables.created_at'),
                        "actions" => __('general.datatables.actions'),
                    ],
                    "url" => route('api.users.datatables')
                ])
            </div>
        </div>
    </x-content>
@endsection