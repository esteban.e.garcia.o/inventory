@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">{{ __('general.' . (isset($item) ? 'edit' : 'add')) }} @lang('general.user')</x-slot>
        <div class="row py-4">
            <div class="col-md-12">
                @component('components.form', [
                    "item" => (isset($item) ? $item : null),
                    "resource" => "users",
                    "back" => route('admin.users.index'),
                    "updateResource" => "user"
                ])
                @include('admin.users.form', ['item' => (isset($item) ? $item : null), "roles" => $roles])
                @endcomponent
            </div>
        </div>        
    </x-content>
@endsection