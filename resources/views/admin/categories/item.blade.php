@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">{{ __('general.' . (isset($item) ? 'edit' : 'add')) }} @lang('general.category')</x-slot>
        <div class="row py-4">
            <div class="col-md-12">
                @component('components.form', [
                    "item" => (isset($item) ? $item : null),
                    "resource" => "categories",
                    "back" => route('admin.categories.index'),
                    "updateResource" => "category"
                ])
                @include('admin.categories.form', ['item' => (isset($item) ? $item : null)])
                @endcomponent
            </div>
        </div>        
    </x-content>
@endsection