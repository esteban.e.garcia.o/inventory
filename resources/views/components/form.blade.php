<form action="{{ (is_null($item) ? route('admin.' . $resource . '.store') : route('admin.' . $resource . '.update', [$updateResource => $item->id])) }}" method="POST" id="form-{{ $resource }}-actions">
    @csrf
    @if (!is_null($item))
        {{ method_field('PUT') }}
    @endif
    {{ $slot }}    
    <div class="form-group">
        <button type="submit" class="btn btn-outline-primary float-right"><i class="fas fa-save"></i> @lang('general.save')</button>
        <a href="{{ $back }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('general.cancel')</a>
    </div>
</form>