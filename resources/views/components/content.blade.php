<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h2>&#x21db; {{ $title }} &#x21da;</h2>
            </div>
            <div class="card-body">
                @if (request('action') && request('action') == "ok")
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>@lang('general.excelent')!</strong> @lang('general.action_success')
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                {{ $slot }}                
            </div>
        </div>
    </div>        
</div>