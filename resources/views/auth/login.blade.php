@extends('layouts.app')

@section('content')
<div class="container">
		<div class="row justify-content-center py-4">
				<div class="card px-0 col-md-12" >
						<div class="row no-gutters px-0">
								<div class="col-md-8">
								<div class="card">
												<div class="card-header text-center">{{ __('auth.info') }}</div>

												<div class="card-body py-4">
														<form id="form-login" method="POST" action="{{ route('login') }}">
																@csrf
																<div class="row">
																	<div class="col-md-12 ">
																		<div class="input-group mb-2 col-md-12 ">	
																			<input id="user_name" placeholder="{{ __('auth.user')}}" type="text" class="form-control @error('user_name') is-invalid @enderror" name="user_name"  autocomplete="current-user_name">
																			<div class="input-group-prepend">
																				<div class="input-group-text"><i class="fas fa-key"></i></div>
																			</div>
																			@error('user_name')
																	
																					<span class="invalid-feedback" role="alert">
																							<strong>{{ $message }}</strong>
																					</span>
																			@enderror
																			
																		</div>
																	</div>	
																</div>
																<div class="row">
																	<div class="col-md-12 ">
																		<div class="input-group mb-2 col-md-12 ">	
																		<input id="password" placeholder="{{ __('auth.password')}}" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="current-password">
																		<div class="input-group-prepend">
																			<div class="input-group-text"><i class="fas fa-key"></i></div>
																		</div>
																			@error('password')
																	
																					<span class="invalid-feedback" role="alert">
																							<strong>{{ $message }}</strong>
																					</span>
																			@enderror
																			
																		</div>
																	</div>	
																</div>

																<div class="form-group row mb-0">
																		<div class="col-md-8 offset-md-4">
																				<button  id="btnLoginForm" class="btn btn-primary col-md-7">
																						{{ __('auth.login') }}
																				</button>

																				
																		</div>
																</div>
														</form>
												</div>
												<div class="card-footer text-muted text-center py-4">
												{{ __('auth.company') }} {{ __('auth.year') }}
												</div>
										</div>
								</div>

								<div class="col-md-4">
										<img src="{{ asset('img/logo-copalist.png')}}" class="card-img" alt="Logo Copalist">
								</div>
						</div>
				</div>
				</div>
				
				</div>
		</div>
</div>
@endsection
