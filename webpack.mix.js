const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js/build')
    .sass('resources/sass/app.scss', 'public/css');
mix.js('resources/js/table.js', 'public/js/build/table.js')
mix.js('resources/js/products.js', 'public/js/build/products.js');
mix.js('resources/js/control.js', 'public/js/build/control.js');
mix.js('resources/js/deliveries.js', 'public/js/build/deliveries.js');
mix.js('resources/js/supply.js', 'public/js/build/supply.js');
mix.js('resources/js/credits.js', 'public/js/build/credits.js');
mix.js('resources/js/materials.js', 'public/js/build/materials.js');
